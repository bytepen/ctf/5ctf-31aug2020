"""
Intel has collected a series of transmissions that seem to resemble a kind of binary
morse code. A translation key has been created but there is *far* too much data to 
translate by hand. You have been tasked with creating an automated tool to translate
the given 'hoarse code' transmission.

Transmission: -..-...-.-.-..---.-.-----.-..---.-.------..-...-.-.-...-----.-.--.-...-.---------..-...---------..-...-.-.-...-------.-.-..-..........--.-.-.-...

Key:
a = .....
b = ....-
c = ...-.
d = ..-..
e = .-...
f = -....
g = ...--
h = ..-.-
i = .-..-
j = -...-
k = .-.--
l = -.-.-
m = --..-
n = -.-..
o = ..---
p = .-.-.
q = -..--
r = .----
s = --.-.
t = -..-.
u = ----.
v = ---..
w = ---.-
x = --.--
y = -.---
z = -.--.
_ = -----

"""
