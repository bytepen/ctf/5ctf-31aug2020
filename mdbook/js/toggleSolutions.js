window.addEventListener('load', () => {
	let btn = document.getElementById('toggle-solutions-button');
	if (localStorage.show_solutions == 'true') {
		details = [...document.getElementsByTagName('details')]
		details.forEach((detail) => {
			detail.open = true;
		});
		btn.textContent = "Hide by Default";
	} else {
		btn.textContent = "Show by Default";
	}
});

function toggleSolutions() {
	let btn = document.getElementById('toggle-solutions-button');
	if (localStorage.show_solutions == 'true') {
		localStorage.removeItem('show_solutions');
		btn.textContent = "Show by Default";
	} else {
		localStorage.setItem('show_solutions', 'true');
		btn.textContent = "Hide by Default";
	}
}
