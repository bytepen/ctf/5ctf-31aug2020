# Boardgames (50 Points)

## Description

Decode the message and submit the flag

```
5tgb76h
6yhnm,
7yhnik,j
65tyhnb
43ewdcv
543edcvb
1qaz3edcw
7yhnik,j
2sx
4rfv5ygn
1qazwdr5tgb
7yhnik,j
4rfvthnmju7
cvbn
78ol,mhy
3edc54f
zxcv
345rfv
5tgb7ujmy
432wsxewvc
vbnm
4rfvgb
67ikmngt
7yhnik,j
4rfv5ygn
2wsx3rfc
1qazsefvgt5
3wsxrfvd
5tgbnm
1qaz3wsc
45tygbv
```

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* Keyboard

### Walkthrough

This one actually took me quite a while of comming back to it before I finally saw the pattern. I noticed that `1qaz` was a waterfall and realized that maybe it had something to do with a keyboard(games).

Sure enough, I trace out `5tgb76h` and realize it kind of looks like the letter `F`. Continuing to do this, I eventually came up with `flag{cHairMan_of_THE_boarDWALK}`. I decided to try the flag in all lower-case and it worked!

Side note: I finally found a practical use for my Corsair RGB Keyboard. Corsair has a utility called iCue that lets you control how the lights work. I configured it to keep the keys that I pressed lit up for a few seconds and was able to use that to help me read the letters as I was typing them.

</details>

## Flags

<details>
<summary>Show Flag</summary>

> ```
> flag{chairman_of_the_boardwalk}
> ```

</details>
