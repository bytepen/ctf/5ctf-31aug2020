# Crypto Challenge (50 Points)

## Description

SSgt Snuffy learned so much about different cipher methods. He feels that he can tackle anything crypto related. He secretly thinks that he can conquer crypto just like Caesar conquered Gaul. One day, SSgt Snuffy's buddy asked him to decrypt the following text. Can you answer the questions before SSgt Snuffy does?

Who said these words?

```
Iulhqgv, Urpdqv, frxqwubphq, ohqg ph brxu hduv;
L frph wr exub Fdhvdu, qrw wr sudlvh klp.
Wkh hylo wkdw phq gr olyhv diwhu wkhp;
Wkh jrrg lv riw lqwhuuhg zlwk wkhlu erqhv;
Vr ohw lw eh zlwk Fdhvdu. Wkh qreoh Euxwxv
Kdwk wrog brx Fdhvdu zdv dpelwlrxv:
Li lw zhuh vr, lw zdv d julhyrxv idxow,
Dqg julhyrxvob kdwk Fdhvdu dqvzhu’g lw.
Khuh, xqghu ohdyh ri Euxwxv dqg wkh uhvw–
Iru Euxwxv lv dq krqrxudeoh pdq;
Vr duh wkhb doo, doo krqrxudeoh phq–
Frph L wr vshdn lq Fdhvdu’v ixqhudo.
Kh zdv pb iulhqg, idlwkixo dqg mxvw wr ph:
Exw Euxwxv vdbv kh zdv dpelwlrxv;
Dqg Euxwxv lv dq krqrxudeoh pdq.
Kh kdwk eurxjkw pdqb fdswlyhv krph wr Urph
Zkrvh udqvrpv glg wkh jhqhudo friihuv iloo:
Glg wklv lq Fdhvdu vhhp dpelwlrxv?
Zkhq wkdw wkh srru kdyh fulhg, Fdhvdu kdwk zhsw:
Dpelwlrq vkrxog eh pdgh ri vwhuqhu vwxii:
Bhw Euxwxv vdbv kh zdv dpelwlrxv;
Dqg Euxwxv lv dq krqrxudeoh pdq.
Brx doo glg vhh wkdw rq wkh Oxshufdo
L wkulfh suhvhqwhg klp d nlqjob furzq,
Zklfk kh glg wkulfh uhixvh: zdv wklv dpelwlrq?
Bhw Euxwxv vdbv kh zdv dpelwlrxv;
Dqg, vxuh, kh lv dq krqrxudeoh pdq.
L vshdn qrw wr glvsuryh zkdw Euxwxv vsrnh,
Exw khuh L dp wr vshdn zkdw L gr nqrz.
Brx doo glg oryh klp rqfh, qrw zlwkrxw fdxvh:
Zkdw fdxvh zlwkkrogv brx wkhq, wr prxuq iru klp?
R mxgjphqw! wkrx duw iohg wr euxwlvk ehdvwv,
Dqg phq kdyh orvw wkhlu uhdvrq. Ehdu zlwk ph;
Pb khduw lv lq wkh friilq wkhuh zlwk Fdhvdu,
Dqg L pxvw sdxvh wloo lw frph edfn wr ph.

Brx duh doprvw wkhuh. Mxvw d ihz folfnv dzdb. Ghflskhu wklv:

LpLz th dh  d,Lfqxugf,v rh

Uhphpehu: Jrrg ihqfhv pdnh jrrg qhljkeruv
```

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* Cyberchef

### Walkthrough

The hints within the description (Julius Caesar crypto) should make it obvious that this is ROT X, where the characters are rotated by X. As with many challenges like this, CyberChef makes quick work of this challenge. I dropped the above text into the `Input` field, and I dropped `ROT13` into the `Recipe` field. This still returned garbage text, so I clicked through to try different `Amount` values. Setting `Amount` to `23` returns the following:

```
Friends, Romans, countrymen, lend me your ears;
I come to bury Caesar, not to praise him.
The evil that men do lives after them;
The good is oft interred with their bones;
So let it be with Caesar. The noble Brutus
Hath told you Caesar was ambitious:
If it were so, it was a grievous fault,
And grievously hath Caesar answer’d it.
Here, under leave of Brutus and the rest–
For Brutus is an honourable man;
So are they all, all honourable men–
Come I to speak in Caesar’s funeral.
He was my friend, faithful and just to me:
But Brutus says he was ambitious;
And Brutus is an honourable man.
He hath brought many captives home to Rome
Whose ransoms did the general coffers fill:
Did this in Caesar seem ambitious?
When that the poor have cried, Caesar hath wept:
Ambition should be made of sterner stuff:
Yet Brutus says he was ambitious;
And Brutus is an honourable man.
You all did see that on the Lupercal
I thrice presented him a kingly crown,
Which he did thrice refuse: was this ambition?
Yet Brutus says he was ambitious;
And, sure, he is an honourable man.
I speak not to disprove what Brutus spoke,
But here I am to speak what I do know.
You all did love him once, not without cause:
What cause withholds you then, to mourn for him?
O judgment! thou art fled to brutish beasts,
And men have lost their reason. Bear with me;
My heart is in the coffin there with Caesar,
And I must pause till it come back to me.

You are almost there. Just a few clicks away. Decipher this:

ImIw qe ae  a,Icnurdc,s oe

Remember: Good fences make good neighbors
```

Poetry is not my strong suit, so I had to google this. I came upon the [wikipedia](https://en.wikipedia.org/wiki/Friends,_Romans,_countrymen,_lend_me_your_ears) article for this poem by Williams Shakespear. This speach was delivered by `Mark Antony`, which is the flag.

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Who said these words?
> ```
> Mark Antony
> ```

</details>
