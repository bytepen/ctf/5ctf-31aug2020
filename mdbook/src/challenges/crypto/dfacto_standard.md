# d-facto standard (50 Points)

## Description

Use the same RSA components from RSA 101. Given the public exponent e = 0x10001, what is private exponent d?

Format example 0x123abc

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* Python

### Walkthrough

If you haven't done so already, consider taking a look through the [Takeaways - RSA](../../takeaways/rsa.md) section of this page, where I go through some of the foundational concepts of RSA and hopefully make all of these challenges easier.

Having completed the challenges up until this point and the information in the challenge description, we have the following:

```
n = 0xa8b19e997c10b0f5a87cdd1fbb922dfcba5e823bebb072faa9e1b665c5f795f68884d6c793c5ccd5ceaeea9b19cb5998701bfe420a387a3f2137dba1004c212495c85e08be015d475406214c19b15b33aa21c3327c6935a9ead91c1d9c4c5e6b38864b46337a34e3cde0c20e45bec187d7e3bdb98b6c993998944f50a041cd690e8843f0808acdad6062dca8785234df5157ffe72263ef7bf25a756af839308d4461daa3f93c20bf5be12fea9bcf5e65586a47aac4ad9066eb157d50fbb9ffc4b4385957aa0c12bdd97dc7f520199bf94fd78e756f56fc88118959935adb6ca81c40d4c564fefca9c9b04ee1b072ed3f661011a9edcd9ff53b6b0d4525eb59bd
p = 0xd9d813795a625231394e0c5a0235cb2669b387e3f1e18e57a5e1fb94ced756b1d07e0c3cf8509a1451b28096d6d33a440abc7d8a957cf9306b41b23c09e167cba537ef87a78ea7037647de97514eadc5a9b01ae601a906aee166c0e2f6714b7f51b21cad0c3d642b1bc45919a0f25a9de635eb745cd4cd0c555a3a61d3be53e7
q = 0xc63db181f57c18b362905103c8c6c3cdc24aef1728e547c54bf76c7c61e314c34335525dc8b2fc68c47d47cb29b6b2b9a08b22935f5a99b9e38060791cd4df01727345436b587dfd6341121cc52c0bdbe6ce7acbf5f2a0a3555334267d1dc68778f8af92ee3fc36478361ce7cf1787a42d673bc3616be879f6d069136b7970bb
r = 0xa8b19e997c10b0f5a87cdd1fbb922dfcba5e823bebb072faa9e1b665c5f795f68884d6c793c5ccd5ceaeea9b19cb5998701bfe420a387a3f2137dba1004c212495c85e08be015d475406214c19b15b33aa21c3327c6935a9ead91c1d9c4c5e6b38864b46337a34e3cde0c20e45bec187d7e3bdb98b6c993998944f50a041cd676e727ef530ac62c8c4847f4aad55a5eb255988ec079d195f00810d59c77ec51830ae7c0938388a4245b167889b457167ad22a78ccfd5fd7c9c536a9bd503b8f79c8d248c9724edbcfff4d741099ee257bf58f8c377bb5535dacf6489e74c5aa1519608856a81d51a35b5d8e040690afd5272ea722f8cea6eef4069cfe6b3951c

e = 0x10001
```

This is where `modinv()` comes in. We can run the above in a python shell, then run the following to add the `modinv()` function:

```
def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    else:
        g, y, x = egcd(b % a, a)
    return (g, x - (b // a) * y, y)

def modinv(a, m):
    g, x, y = egcd(a, m)
    if g != 1:
        raise Exception('modular inverse does not exist')
    else:
        return x % m
```

Then we can run the following to get our flag:

```
d = modinv(e, r)
print(hex(d))
```

This outputs the following:

```
0x6cf2114084ac6eeca83cc3be1bb0637cc58c05c5d70824a9980f4a71ddf170eb3fedb29c3fe64dcc8367d9e2033d9e0661d8e2531a30b15a960472163f8feb13aa9c3586a5d751bba01b3963e16ff63b9e93a171b8816603efc4f9707221ac14c8f38a27b35528553afffdb3a3fd7715592b1ebb76bcd3142c6313fa39156627776000782f5015981dba884edd34e42bc1fa5a003d468b3d1ca13bd31090e18f26d97907a744749e2a4e51541b1b350b44f88d86d62f183f62e248868d8292f16f2c843f3bd9c0a40015caf298d900cab4ac2c3468de1640eaa86800dbdd26e6ea3ee00ff1e219938cbe7d6b0e8e0867e3d88d6ae324b9be9d4b574aca6e8e4d
```

 
</details>

## Flags

<details>
<summary>Show Flag</summary>

> What is private exponent d?
> ```
> 0x6cf2114084ac6eeca83cc3be1bb0637cc58c05c5d70824a9980f4a71ddf170eb3fedb29c3fe64dcc8367d9e2033d9e0661d8e2531a30b15a960472163f8feb13aa9c3586a5d751bba01b3963e16ff63b9e93a171b8816603efc4f9707221ac14c8f38a27b35528553afffdb3a3fd7715592b1ebb76bcd3142c6313fa39156627776000782f5015981dba884edd34e42bc1fa5a003d468b3d1ca13bd31090e18f26d97907a744749e2a4e51541b1b350b44f88d86d62f183f62e248868d8292f16f2c843f3bd9c0a40015caf298d900cab4ac2c3468de1640eaa86800dbdd26e6ea3ee00ff1e219938cbe7d6b0e8e0867e3d88d6ae324b9be9d4b574aca6e8e4d
> ```

</details>
