# Disorderly (500 Points)

## Description

Decode the following cipher to get the flag. This is a homegrown cipher, so don't expect to work it out with cyberchef alone.

```
blvsqofkazbdgysfocvcofrvhfbsnwoajdwmlecxdasjhzeyeiobzfckvjeucysfaipvexqetavkcfsxbhewcfjxnfsaxepbhyeyboupevtecyfaphmzfobuyewgfcukzaftrlmzshfbnzfwufazexpidbwisafnwrajfmyfwbopdzqjdbotvfqicwfzclftgvoukbepxgodtaweaphvfyakosfwcfolruwdkranvscenxicejuxfojqxewfvukaveqmavezkafuiwjrtoeaxbnsiexnrftychdpwaemtzfygfbrwnfcswsbeyfboysfjbwmcrekyaoufrzfxdnicvkaeyltdbzdrhatxdmlvfvbgofxeblwdumcxokfpavezdubjxfcnhyfzgpumdbwtcezsrbhdvfjncqvqsnezicdovdyinbwjubpmwtrcnkvevajdzadswfhpbzmfaxrmlewfxeyhcozekaysmdaxdzgorbuxcejvkfbuwamdiyuamfyubpzosbeylodavkbdmyfybpgftxdtayjfbtxnfcvfydnahxjcfxbufxcimewdxsblvfwtbrdizbkdvlotdbpyafitnzolfbzmfayaqmdkyftpovhfbnydwucfxeclmwtierbznbdtlpydxbouidyfauhpwbnteyfzsebwdypaiudwlbdvhebuoyaerowofpayofcjwcnuexndcyeyetbvlmcdwicfydakybeuweqcvtbmfvicndzrlndvfzexcqybeunivofchwfwftblwmeqcxdzgfboxfalwkbueypiaeuxdytqbfvfbkxsfcxfckmwndbjxmtdcvmlcfpwsrmecwadouyfydmcsrxafmsvcifovgsfapzdyauheqyucjofpxncjexmepicvadnsxdaowfwufcnvkdanrxnkurbdvcgmetyaertmxlbdvfwcdinztflavdoatjwscemxhsqcdxlofzexlubxptfoyecjmzjnfbzdxhebwisfcovfbiozfxtajfvgfoaweyukdbqnyumfhcxeobixmqfjxezhaspydjopubwdwifltbylfmrauyugbmexewaeqvodcsvmhdcwfnahzfsobyebqgvdxcdptmxdtnaweibozdzagtfrzimdcxubdzfgrayhodcyobfswocdweuvqlwjqvtiakvearnluxmsfcgzrbidxdvmafqyjamfwbduzuceorvdxkbetysgcdrzexaermycmjfzdsazarfsmxkfrtchzspdiazalfyshfcnvpbndxcrdmybmkfzadjtvrokecxfqotbxmgsbpkxcudzdqbixndscrkxhnsebzbpojfvmaevnhkqubyeksawrgseaxugcnjqxrandvsobhdymdqbjxbjpdozsfgkbwbtgmejyqlwjrx
```

## Solution

<details>
<summary>Show Solution</summary>

### Hints

* 1 cleartext can encode into several different ciphertexts. Each of those ciphertexts decodes to the same original cleartext.
* It's delimited by 1 of 5 characters.

### Tools

* Python

### Walkthrough

I'm going to start this off by saying that I did not come up with the solution to this challenge.
Nobody actually solved this challenge within the CTF and the creator released his script for encrypting/decrypting text for this challenge.

That said, I figured I could at least walk through his script and explain it.
Here is his script:

```
#!/usr/bin/python
from random import choice, shuffle
from re import split as resplit

bits = { 64 : 'abc',
	     32 : 'def',
	     16 : 'ghi',
	      8 : 'jkl',
	      4 : 'mno',
	      2 : 'pqr',
	      1 : 'stu'
}

endbit = 'vwxyz'

def encrypt(message):
    output = ''
    for letter in range(len(message)):
        letter = message[letter]
        crypt_letter = ''
        for bit in bits.keys():
            if ord(letter) & bit:
                crypt_letter += choice(bits[bit])
        crypt_letter = list(crypt_letter)
        shuffle(crypt_letter)
        crypt_letter = ''.join(crypt_letter) + choice(endbit)
        output += crypt_letter
    return output

def decrypt(message):
    letters = resplit('[' + endbit + ']', message)
    output = ''
    for letter in letters:
        byte_value = 0
        for part in range(len(letter)):
            for k,v in bits.iteritems():
                if letter[part] in v:
                   byte_value += k
                   break
        if byte_value:
            output += chr(byte_value)
    return output
```

To better understand what is going on, I'm going to convert the plaintext `hello` into the ciphertext `aflvfbtoyebolxmjafxkunbqey`.

There are two functions within this; `encrypt` and `decrypt`.
What they output should be obvious, but how they get there may not be.
I will explain below.

### encrypt()

This function takes in plaintext as an argument and returns the ciphertext.
It goes through each letter in the message one at a time alongside each key from the bits dictionary (`bits.keys() == [64, 32, 16, 8, 4, 2, 1]`).
It converts the letter into its ordinal number `ord()` and performs an and (`&`) operation on them.
If they have overlapping bits, it adds a random character (`choice(bits[bit])`) from that block to the encrypted string for this letter.
It then mixes them around (`shuffle(crypt_letter)`) and turns them into a string suffixed by a random letter from the endbit variable (`choice(endbits)`).
This is then performed for each character.

I know that's a lot to take in, so let's go through an example.
Let's take the letter `h`, which has the ordinal value of `104`.

| number | binary     |
| ------ | ---------- |
| `104`  | `01101000` |
| `64`   | `01000000` |
| `32`   | `00100000` |
| `16`   | `00010000` |
| `8`    | `00001000` |
| `4`    | `00000100` |
| `2`    | `00000010` |
| `1`    | `00000001` |

Now that we have that mapped out, `&` simply returns the bits in which both numbers being compared have a `1`.
So if we look at the table able, we can tell that `104 & 64` both have the second bit as `1`.
This gives us `01000000` or `64`.
The python `if` statement evaluates this as `True`, so it performs the code within.

This code selects a character from `bits[64]`, which has the options of `a`, `b`, and `c`. In my `hello` example above, the letter `a` was chosen. It then repeats this for all the locations that have a `1`.
This is `32` (so `f` was chosen) and `8` (so `l` was chosen).

Finally, a random character from the endbit is chosen.
These are `vwxyz`.
In my example, `v` was chosen.

Now all of our letters (`afl`) are jumbled (in this case, they randomly happened to be in their original order) and an endbit letter is randomly chosen (`v`).
This makes our `h` into `aflv`, but it could have just as easily been `ecjz` or `kafy` or any other combination of letters from the bits and endbit varibles.

From that point on, it just does this process to every single letter, then returns the full mess of letters.

### decrypt()

Hopefully you were able to follow what we did above, because now we're basically going to do it in reverse.

We know that the delimiters are the characters within the `endbit` variable, so we are using regex to split this mess of a line into its individual characters first.
In my "hello" example, this results in `['afl', 'fbto', 'ebol', 'mjaf', 'kunbqe']`
Now that we have that, we go through the bits dictionary and determine if the string contains a letter from each item in the dictionary.
In the case of `afl`, it contains an item from `64`, `32`, and `8`, so these 3 numbers are added together to get `104`.
`chr(104)` is "h", so "h" is added to the `output` and we move onto the next letter. Once this is done, we have our string.

### Solution

Now that you understand what's going on here, we can take the string from our challenge and run it through the decrypt function by adding the following line to the python code from above:

```
print(decrypt("..."))
```

We can then run our script and we will see the following message:

> Hopefully this cipher gave you a run for your money.  If you're reading this, then it wasn't TOO hard.  The WhiteCell said that I shouldn't also use a shuffled alphabet.  But if this challenge gets solved enough times, I'll put it out. So you better get started!
> 
> Your flag is flag{shuffling_around_is_funny}

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Flag from the encoded message
> ```
> flag{shuffling_around_is_funny}
> ```

</details>
