# Easy e

## Description

RSA has 2 very common public exponents. Multiply them together and submit them as hex. Format: 0x123abc

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* Python

### Walkthrough

If you haven't done so already, consider taking a look through the [Takeaways - RSA](../../takeaways/rsa.md) section of this page, where I go through some of the foundational concepts of RSA and hopefully make all of these challenges easier.

As explained in `RSA Takeaways`, `e` is usually `3` or `65537`. Execute the following python to get the flag:

```
hex(3*65537)
```

This outputs `0x30003`.

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Multiply the 2 very common public exponents (`e`) and submit the answer as hex
> ```
> 0x30003
> ```

</details>
