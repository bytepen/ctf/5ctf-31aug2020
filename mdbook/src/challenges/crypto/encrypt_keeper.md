# enCrypt Keeper (50 Points)

## Description

Use the same RSA components from RSA 101. Encrypt the string inside the quotes. 'rsa_is_cool'

Note: Don't pad Format example: 0x123abc

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* python
    * pycryptodome (`pip3 install pycryptodome`)

### Walkthrough

If you haven't done so already, consider taking a look through the [Takeaways - RSA](../../takeaways/rsa.md) section of this page, where I go through some of the foundational concepts of RSA and hopefully make all of these challenges easier.

This challenge requests that, given all the previous information, we encrypt `rsa_is_cool` and submit it as hex. The easiest was I found to do this is via python's pycryptodome library, so make sure you install that with `pip3 install pycryptodome` if you haven't already. After that, import all the previous stuff with the following:

```
n = 0xa8b19e997c10b0f5a87cdd1fbb922dfcba5e823bebb072faa9e1b665c5f795f68884d6c793c5ccd5ceaeea9b19cb5998701bfe420a387a3f2137dba1004c212495c85e08be015d475406214c19b15b33aa21c3327c6935a9ead91c1d9c4c5e6b38864b46337a34e3cde0c20e45bec187d7e3bdb98b6c993998944f50a041cd690e8843f0808acdad6062dca8785234df5157ffe72263ef7bf25a756af839308d4461daa3f93c20bf5be12fea9bcf5e65586a47aac4ad9066eb157d50fbb9ffc4b4385957aa0c12bdd97dc7f520199bf94fd78e756f56fc88118959935adb6ca81c40d4c564fefca9c9b04ee1b072ed3f661011a9edcd9ff53b6b0d4525eb59bd
p = 0xd9d813795a625231394e0c5a0235cb2669b387e3f1e18e57a5e1fb94ced756b1d07e0c3cf8509a1451b28096d6d33a440abc7d8a957cf9306b41b23c09e167cba537ef87a78ea7037647de97514eadc5a9b01ae601a906aee166c0e2f6714b7f51b21cad0c3d642b1bc45919a0f25a9de635eb745cd4cd0c555a3a61d3be53e7
q = 0xc63db181f57c18b362905103c8c6c3cdc24aef1728e547c54bf76c7c61e314c34335525dc8b2fc68c47d47cb29b6b2b9a08b22935f5a99b9e38060791cd4df01727345436b587dfd6341121cc52c0bdbe6ce7acbf5f2a0a3555334267d1dc68778f8af92ee3fc36478361ce7cf1787a42d673bc3616be879f6d069136b7970bb
r = 0xa8b19e997c10b0f5a87cdd1fbb922dfcba5e823bebb072faa9e1b665c5f795f68884d6c793c5ccd5ceaeea9b19cb5998701bfe420a387a3f2137dba1004c212495c85e08be015d475406214c19b15b33aa21c3327c6935a9ead91c1d9c4c5e6b38864b46337a34e3cde0c20e45bec187d7e3bdb98b6c993998944f50a041cd676e727ef530ac62c8c4847f4aad55a5eb255988ec079d195f00810d59c77ec51830ae7c0938388a4245b167889b457167ad22a78ccfd5fd7c9c536a9bd503b8f79c8d248c9724edbcfff4d741099ee257bf58f8c377bb5535dacf6489e74c5aa1519608856a81d51a35b5d8e040690afd5272ea722f8cea6eef4069cfe6b3951c
e = 0x10001
d = 0x6cf2114084ac6eeca83cc3be1bb0637cc58c05c5d70824a9980f4a71ddf170eb3fedb29c3fe64dcc8367d9e2033d9e0661d8e2531a30b15a960472163f8feb13aa9c3586a5d751bba01b3963e16ff63b9e93a171b8816603efc4f9707221ac14c8f38a27b35528553afffdb3a3fd7715592b1ebb76bcd3142c6313fa39156627776000782f5015981dba884edd34e42bc1fa5a003d468b3d1ca13bd31090e18f26d97907a744749e2a4e51541b1b350b44f88d86d62f183f62e248868d8292f16f2c843f3bd9c0a40015caf298d900cab4ac2c3468de1640eaa86800dbdd26e6ea3ee00ff1e219938cbe7d6b0e8e0867e3d88d6ae324b9be9d4b574aca6e8e4d
```

After that, we can get our flag with the following:

```
from Crypto.PublicKey import RSA

pri = RSA.construct((n, e, d, p, q))
cipher = int(pri.encrypt(b'rsa_is_cool', 0)[0].hex(), 16)
print(hex(cipher))
```

This should output the following:

```
0x20ee00c90f317b10fb41c896bc7395cbf7d83619323f7ea4ad020b95f350123deefebb8da3cef0fe043c3e452028a13e16700f7ddc079aaef7a31e6f1d6115dce2a21ce0adcb8ea4f7a65a4d102e5edbdac3d2f8f1f20a909eea89994fd21cee85f9714b17dc6f5a0074d4e9a94d3b3431295f74eb9b1f44c367d8fd370978454676b24c08a80bf7d9682c603371e3a17f32c91cc84fc73af510e0c456e7983d03ce185db0f07c1c60b3dc7ebc6a718def9440ecf1ea840a40e225578a3065cbe0b908d56dc33db85192e1dc4c9cfe7d9ebdb5480282191f792ac430750717436aecf973681f3bed462fbae16596df94b169f60a1aaca422d36f007fdfe85722
```

### Creator Notes

The creator of this challenge wanted me to note that you could also forgo using `pycryptodome` and do this in cold, hard MATH!
The formula to get the ciphertext from a message is `c = (m ^ e) % n`, so if we add all of our variables above, we can get the same flag with the following additional python:

```
m = int(b'rsa_is_cool'.hex(), 16)
c = pow(m,e,n)
hex(c)
```

Note the use of the `pow(m, e, n)` function.
This function does the python equivilent of `(m ** e) % n`, however, in certain circumstances, the later can take forever.
`pow()` takes just a few seconds at most.

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Encrypt the string inside the quotes: 'rsa_is_cool'
> ```
> 0x20ee00c90f317b10fb41c896bc7395cbf7d83619323f7ea4ad020b95f350123deefebb8da3cef0fe043c3e452028a13e16700f7ddc079aaef7a31e6f1d6115dce2a21ce0adcb8ea4f7a65a4d102e5edbdac3d2f8f1f20a909eea89994fd21cee85f9714b17dc6f5a0074d4e9a94d3b3431295f74eb9b1f44c367d8fd370978454676b24c08a80bf7d9682c603371e3a17f32c91cc84fc73af510e0c456e7983d03ce185db0f07c1c60b3dc7ebc6a718def9440ecf1ea840a40e225578a3065cbe0b908d56dc33db85192e1dc4c9cfe7d9ebdb5480282191f792ac430750717436aecf973681f3bed462fbae16596df94b169f60a1aaca422d36f007fdfe85722
> ```

</details>
