# Crypto

The crypto section contained 20 challenges worth a total of 1800 points.

I typically have a pretty rough time with crypto, but I kind of enjoyed this category this time around.
A handful of them were pretty easy.
Most of them were RSA, which I previously knew nothing about.
Now I feel like I know too much about it.
It was pretty painful trying to parse through the endless threads with math symbols I didn't understand slapped together in ways that made me want to tear my arm off and beat the computer with it, but once I figure out what on Earth they were talking about, I feel like I was able to put the information into English over on the RSA Takeaways page.

