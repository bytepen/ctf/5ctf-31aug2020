# Lack of Powers (50 Points)

## Description

Using the public key from RSA 102, what's the encryption exponent? Format 0x123abc

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* python
  * pycryptodome

This is a very basic continuation of `RSA 102`. We are given the following public key:

```
-----BEGIN PUBLIC KEY-----
MIIBIDANBgkqhkiG9w0BAQEFAAOCAQ0AMIIBCAKCAQEAkvwrbIxXP+8oIfY24CPH
4J6a+yDwgrJSDAey5yjqQrEnBO4iEJ7mDVwBEcc2tFIxJLNt6nV7Qlw02pUSjeqq
/RHvTbZdkMAuS306Rxhj54rGhzhC5i3Tz+IDYjddOzFqFopBUC5hB3GqFsiHheiM
gkNto/viZHhoxQ6Z4x00NINcldf9QQ9mGKdj/PQXr3k2SVxF6BOH0MjMe14ZVJX6
a7kIzjVKAr5SAbh4nVsTuQGJkyf69gCbjOChuN/DRIajo0V6csmW7lzv8ol5LCO6
hl7XP9v3EI5+o34qaHauM04rHcaCSEwjcnZa1QD8f9cTl+x14RY6FpDMYmW3nJ1Q
UwIBAw==
-----END PUBLIC KEY-----
```

We put it into a file. I'll call this file `rsa.pub` and we run the following code:

```
from Crypto.PublicKey import RSA

pub = RSA.importKey(open('rsa.pub').read())
hex(pub.e)
```

This outputs `0x3`.

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Encryption Exponent
> ```
> 0x3
> ```

</details>
