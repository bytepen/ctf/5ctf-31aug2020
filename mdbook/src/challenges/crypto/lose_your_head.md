# Lose Your Head (50 Points)

## Description

Oh no! I bumped my head again and I forgot some things :(

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* GIMP

### Walkthrough

In this challenge, we are given a file and some hints that the header of the image is corrupt.
This is a pretty straight forward challenge to solve, but it does take some luck.

Basically, all of the image data is still there, but the information that computers normally use to access it properly is gone.
More powerful image editors (such as GIMP) have the ability to just "trust" that what you're giving them is actually an image and they'll try to work with you to read the data from it.

The first step is to rename the image from `lose_your_head.bmp` to `lose_your_head.data`. If GIMP has already made the association, you may be able to just double click on the image at this point.
If not, open GIMP and go to `File` => `Open` and open `lose_your_head.ata`.

You will see a screen that looks something like this:

![](../../images/lose_your_head-001.png)

The data is in there, you just have to work for it.
The goal is to find anything that kind of looks like the text or other data you're looking for.
I first mess with the offset slider and if nothing stick out, then I'll mess around with `width` and `height`.
Usually, these three are the only things you need to touch.

Eventually, you will start to see something that looks like your data.
When this happens, note your current values in case it screws up so you have a place to start from.

![](../../images/lose_your_head-002.png)

With just a little more tweaking, you'll see your image.

![](../../images/lose_your_head-003.png)

Note that it's upside down due to how images are stored in bmp files.
I never seem to have much luck clicking the `Open` button, but if it really bothers you, or is very hard to understand, you can take a screenshot and rotate it around until it looks right.

![](../../images/lose_your_head-004.png)

In this case, we can see our flag `flag{composition_nightmare_10080}`.

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Flag in Image
> ```
> flag{composition_nightmare_10080}
> ```

</details>
