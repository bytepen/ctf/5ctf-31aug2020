# National Broadcast (50 Points)

## Description

We have obtained 3 encrypted messages encrypted with separate public keys, but they all have the same exponent. We believe the messages are identical. Decrypt them for the flag.

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* python
  * pycryptodome

### Walkthroughs

These criminals give me a certain sense of `It's Always Sunny in Philadelphia`.

![](../../images/playing_both_sides.png)

You're given a few files called `badguy[1-3].pub` and `badguy[1-3].enc`. The only problem is that the badguys put all the values required for their private key inside `badguy[1-3].pub` so the good guys can easily decrypt their messages.

Taking the following from `badguy1`, we have:

> badguy1.pub
> ```
> RSA Private-Key: (2048 bit, 2 primes)
> modulus:
>     00:92:fc:2b:6c:8c:57:3f:ef:28:21:f6:36:e0:23:
>     c7:e0:9e:9a:fb:20:f0:82:b2:52:0c:07:b2:e7:28:
>     ea:42:b1:27:04:ee:22:10:9e:e6:0d:5c:01:11:c7:
>     36:b4:52:31:24:b3:6d:ea:75:7b:42:5c:34:da:95:
>     12:8d:ea:aa:fd:11:ef:4d:b6:5d:90:c0:2e:4b:7d:
>     3a:47:18:63:e7:8a:c6:87:38:42:e6:2d:d3:cf:e2:
>     03:62:37:5d:3b:31:6a:16:8a:41:50:2e:61:07:71:
>     aa:16:c8:87:85:e8:8c:82:43:6d:a3:fb:e2:64:78:
>     68:c5:0e:99:e3:1d:34:34:83:5c:95:d7:fd:41:0f:
>     66:18:a7:63:fc:f4:17:af:79:36:49:5c:45:e8:13:
>     87:d0:c8:cc:7b:5e:19:54:95:fa:6b:b9:08:ce:35:
>     4a:02:be:52:01:b8:78:9d:5b:13:b9:01:89:93:27:
>     fa:f6:00:9b:8c:e0:a1:b8:df:c3:44:86:a3:a3:45:
>     7a:72:c9:96:ee:5c:ef:f2:89:79:2c:23:ba:86:5e:
>     d7:3f:db:f7:10:8e:7e:a3:7e:2a:68:76:ae:33:4e:
>     2b:1d:c6:82:48:4c:23:72:76:5a:d5:00:fc:7f:d7:
>     13:97:ec:75:e1:16:3a:16:90:cc:62:65:b7:9c:9d:
>     50:53
> publicExponent: 3 (0x3)
> privateExponent:
>     61:fd:72:48:5d:8f:7f:f4:c5:6b:f9:79:ea:c2:85:
>     40:69:bc:a7:6b:4b:01:cc:36:b2:af:cc:9a:1b:46:
>     d7:20:c4:ad:f4:16:b5:bf:44:08:e8:00:b6:84:cf:
>     22:e1:76:18:77:9e:9c:4e:52:2c:3d:78:91:b8:b7:
>     09:47:1c:a8:b6:9f:89:24:3e:60:80:1e:dc:fe:26:
>     da:10:42:9a:5c:84:5a:25:81:ee:c9:37:df:ec:02:
>     41:7a:3e:27:76:46:b9:b1:80:e0:1e:eb:5a:4b:c6:
>     b9:db:05:03:f0:5d:ac:2c:f3:c2:a7:ec:42:fa:f0:
>     83:5f:11:42:13:78:23:01:3b:14:f2:53:83:c7:c5:
>     17:20:c7:0d:13:7c:04:cf:42:05:28:4a:f1:ab:51:
>     89:96:46:7a:ec:79:df:86:09:ea:99:54:ff:08:e9:
>     83:36:58:32:39:3c:9a:84:16:a9:66:06:01:91:e3:
>     ce:bc:58:bf:84:8c:7d:c6:b3:e1:21:1b:51:58:49:
>     81:85:2b:ec:a9:76:30:98:27:b3:fe:bb:c6:4b:55:
>     bc:f1:3c:d9:f2:4c:c6:14:d3:2e:e3:f9:d1:92:59:
>     ef:e7:4d:eb:8d:9e:68:46:ea:74:d6:45:76:bb:39:
>     36:5b:30:22:ab:77:d9:15:6a:b8:6d:eb:0c:68:15:
>     eb
> prime1:
>     00:c2:bd:20:b6:53:36:24:a2:60:e0:03:e4:8f:9b:
>     03:3f:25:9f:7f:73:f7:61:22:05:a5:50:dd:89:87:
>     32:6a:a3:d4:91:28:6b:98:92:9e:a9:42:e6:16:68:
>     62:73:fa:8d:3f:57:a7:41:c4:26:b3:4b:b9:18:b2:
>     60:8a:66:f0:f7:6e:4a:f5:72:c3:5a:a7:2b:73:76:
>     3f:a5:fb:17:fa:70:28:52:c1:f0:08:f4:0c:16:85:
>     bc:40:49:42:a5:08:54:8f:93:61:71:69:38:19:12:
>     04:a4:25:ed:1c:0e:e2:0c:a8:e6:71:cb:26:34:24:
>     6c:df:60:fb:d6:cc:9b:a7:03
> prime2:
>     00:c1:39:4b:c9:a8:2d:99:d3:95:59:65:72:4e:0d:
>     3f:14:1c:00:56:09:9b:2c:60:61:bd:72:1d:d8:fe:
>     1a:80:e7:fe:78:27:3c:53:2b:4e:24:73:7c:87:4d:
>     32:7d:c0:5b:41:39:25:e3:7b:bf:63:21:e0:b6:49:
>     d5:2b:0b:e4:03:3a:f6:16:bd:be:7a:64:33:4b:33:
>     65:97:a3:0d:a6:6c:c5:83:e2:82:12:d5:96:f4:f4:
>     a2:ad:58:de:12:70:9e:14:a2:8a:1b:fd:bf:9c:c4:
>     07:56:ff:d2:78:3e:dc:31:1d:7d:3b:e1:ee:d2:2c:
>     83:cc:ec:c5:00:3d:65:88:71
> exponent1:
>     00:81:d3:6b:24:37:79:6d:c1:95:ea:ad:43:0a:67:
>     57:7f:6e:6a:54:f7:fa:40:c1:59:18:e0:93:b1:04:
>     cc:47:17:e3:0b:70:47:bb:0c:69:c6:2c:99:64:45:
>     96:f7:fc:5e:2a:3a:6f:81:2d:6f:22:32:7b:65:cc:
>     40:5c:44:a0:a4:f4:31:f8:f7:2c:e7:1a:1c:f7:a4:
>     2a:6e:a7:65:51:a0:1a:e1:d6:a0:05:f8:08:0f:03:
>     d2:d5:86:2c:6e:05:8d:b5:0c:eb:a0:f0:d0:10:b6:
>     ad:c2:c3:f3:68:09:ec:08:70:99:a1:32:19:78:18:
>     48:94:eb:52:8f:33:12:6f:57
> exponent2:
>     00:80:d0:dd:31:1a:c9:11:37:b8:e6:43:a1:89:5e:
>     2a:0d:68:00:39:5b:bc:c8:40:41:28:f6:be:90:a9:
>     67:00:9a:a9:a5:6f:7d:8c:c7:89:6d:a2:53:04:de:
>     21:a9:2a:e7:80:d0:c3:ec:fd:2a:42:16:95:ce:db:
>     e3:72:07:ed:57:7c:a4:0f:29:29:a6:ed:77:87:77:
>     99:0f:c2:09:19:9d:d9:02:97:01:61:e3:b9:f8:a3:
>     17:1e:3b:3e:b6:f5:be:b8:6c:5c:12:a9:2a:68:82:
>     af:8f:55:36:fa:d4:92:cb:68:fe:27:eb:f4:8c:1d:
>     ad:33:48:83:55:7e:43:b0:4b
> coefficient:
>     66:de:3e:a2:be:66:eb:ba:b9:b2:ac:32:b2:9c:c5:
>     bf:60:e7:77:4a:2a:0c:ee:ad:27:2d:02:55:e3:50:
>     b0:62:cf:0c:50:f9:1f:50:bb:42:d6:07:54:4c:98:
>     31:de:42:80:c5:6a:c1:4b:fe:31:ca:00:24:7f:48:
>     20:29:81:5c:46:07:13:b6:6f:75:0d:89:37:c4:46:
>     ce:e2:d2:a7:26:b0:85:cf:2b:5f:c2:c2:e0:00:a0:
>     37:bc:5e:8d:de:af:4a:0b:db:c0:c3:46:6f:b7:d4:
>     38:b4:82:4d:08:df:54:69:68:6a:0b:0a:d8:d6:24:
>     41:e2:f6:6f:cd:2c:2a:46
> Modulus=92FC2B6C8C573FEF2821F636E023C7E09E9AFB20F082B2520C07B2E728EA42B12704EE22109EE60D5C0111C736B4523124B36DEA757B425C34DA95128DEAAAFD11EF4DB65D90C02E4B7D3A471863E78AC6873842E62DD3CFE20362375D3B316A168A41502E610771AA16C88785E88C82436DA3FBE2647868C50E99E31D3434835C95D7FD410F6618A763FCF417AF7936495C45E81387D0C8CC7B5E195495FA6BB908CE354A02BE5201B8789D5B13B901899327FAF6009B8CE0A1B8DFC34486A3A3457A72C996EE5CEFF289792C23BA865ED73FDBF7108E7EA37E2A6876AE334E2B1DC682484C2372765AD500FC7FD71397EC75E1163A1690CC6265B79C9D5053
> -----BEGIN PUBLIC KEY-----
> MIIBIDANBgkqhkiG9w0BAQEFAAOCAQ0AMIIBCAKCAQEAkvwrbIxXP+8oIfY24CPH
> 4J6a+yDwgrJSDAey5yjqQrEnBO4iEJ7mDVwBEcc2tFIxJLNt6nV7Qlw02pUSjeqq
> /RHvTbZdkMAuS306Rxhj54rGhzhC5i3Tz+IDYjddOzFqFopBUC5hB3GqFsiHheiM
> gkNto/viZHhoxQ6Z4x00NINcldf9QQ9mGKdj/PQXr3k2SVxF6BOH0MjMe14ZVJX6
> a7kIzjVKAr5SAbh4nVsTuQGJkyf69gCbjOChuN/DRIajo0V6csmW7lzv8ol5LCO6
> hl7XP9v3EI5+o34qaHauM04rHcaCSEwjcnZa1QD8f9cTl+x14RY6FpDMYmW3nJ1Q
> UwIBAw==
> -----END PUBLIC KEY-----
> ```
>
> badguy1.enc
> ```
> 0x20b77e4dbd5cef24e5bf0388ba467e247e1da2963e521bd9c577c5237a2b7b53f7b5755e95880595286f00b85237e3628ac27360d7e4cdd69125511c15db5ab31f37a8f6d81909a0b62a3f8d6083a61289801d60730a260658b3d12d521bfdb5977e2b05d5db42db5dff9c2192512a3834992a57c95bb88b4f863533810609fb9da993441e3641d46cb97ce7387dccae6f7802000252a81b7a2b156eb2d7da4dd2b920c11c0a2979f3ae5f44289a5c529bc6417d492faa52c0f13e0c3dda4f5cfdd63fd66f0fac79dfe635fcec7ce53843dc5c3c81587558a43cb0744b2c3dbb4b3d17a53be0c628c497455bfb2bb3e297a129007e5a648ca18d46521e43ea6a
> ```

These aren't in a SUPER easy to use format, but we can work that out with a little bash magic:

```
k='00:92:fc:2b:6c:8c:57:3f:ef:28:21:f6:36:e0:23:
    c7:e0:9e:9a:fb:20:f0:82:b2:52:0c:07:b2:e7:28:
    ea:42:b1:27:04:ee:22:10:9e:e6:0d:5c:01:11:c7:
    36:b4:52:31:24:b3:6d:ea:75:7b:42:5c:34:da:95:
    12:8d:ea:aa:fd:11:ef:4d:b6:5d:90:c0:2e:4b:7d:
    3a:47:18:63:e7:8a:c6:87:38:42:e6:2d:d3:cf:e2:
    03:62:37:5d:3b:31:6a:16:8a:41:50:2e:61:07:71:
    aa:16:c8:87:85:e8:8c:82:43:6d:a3:fb:e2:64:78:
    68:c5:0e:99:e3:1d:34:34:83:5c:95:d7:fd:41:0f:
    66:18:a7:63:fc:f4:17:af:79:36:49:5c:45:e8:13:
    87:d0:c8:cc:7b:5e:19:54:95:fa:6b:b9:08:ce:35:
    4a:02:be:52:01:b8:78:9d:5b:13:b9:01:89:93:27:
    fa:f6:00:9b:8c:e0:a1:b8:df:c3:44:86:a3:a3:45:
    7a:72:c9:96:ee:5c:ef:f2:89:79:2c:23:ba:86:5e:
    d7:3f:db:f7:10:8e:7e:a3:7e:2a:68:76:ae:33:4e:
    2b:1d:c6:82:48:4c:23:72:76:5a:d5:00:fc:7f:d7:
    13:97:ec:75:e1:16:3a:16:90:cc:62:65:b7:9c:9d:
    50:53'
echo $k | tr -d "\n :"
```

The above will output the following:

```
0092fc2b6c8c573fef2821f636e023c7e09e9afb20f082b2520c07b2e728ea42b12704ee22109ee60d5c0111c736b4523124b36dea757b425c34da95128deaaafd11ef4db65d90c02e4b7d3a471863e78ac6873842e62dd3cfe20362375d3b316a168a41502e610771aa16c88785e88c82436da3fbe2647868c50e99e31d3434835c95d7fd410f6618a763fcf417af7936495c45e81387d0c8cc7b5e195495fa6bb908ce354a02be5201b8789d5b13b901899327faf6009b8ce0a1b8dfc34486a3a3457a72c996ee5ceff289792c23ba865ed73fdbf7108e7ea37e2a6876ae334e2b1dc682484c2372765ad500fc7fd71397ec75e116
```

Repeat this for all of the variables we need and we will soon be able to create the following python code:

```
from Crypto.PublicKey import RSA

n = 0x0092fc2b6c8c573fef2821f636e023c7e09e9afb20f082b2520c07b2e728ea42b12704ee22109ee60d5c0111c736b4523124b36dea757b425c34da95128deaaafd11ef4db65d90c02e4b7d3a471863e78ac6873842e62dd3cfe20362375d3b316a168a41502e610771aa16c88785e88c82436da3fbe2647868c50e99e31d3434835c95d7fd410f6618a763fcf417af7936495c45e81387d0c8cc7b5e195495fa6bb908ce354a02be5201b8789d5b13b901899327faf6009b8ce0a1b8dfc34486a3a3457a72c996ee5ceff289792c23ba865ed73fdbf7108e7ea37e2a6876ae334e2b1dc682484c2372765ad500fc7fd71397ec75e1163a1690cc6265b79c9d5053
e = 0x3
d = 0x61fd72485d8f7ff4c56bf979eac2854069bca76b4b01cc36b2afcc9a1b46d720c4adf416b5bf4408e800b684cf22e17618779e9c4e522c3d7891b8b709471ca8b69f89243e60801edcfe26da10429a5c845a2581eec937dfec02417a3e277646b9b180e01eeb5a4bc6b9db0503f05dac2cf3c2a7ec42faf0835f1142137823013b14f25383c7c51720c70d137c04cf4205284af1ab518996467aec79df8609ea9954ff08e983365832393c9a8416a966060191e3cebc58bf848c7dc6b3e1211b51584981852beca976309827b3febbc64b55bcf13cd9f24cc614d32ee3f9d19259efe74deb8d9e6846ea74d64576bb39365b3022ab77d9156ab86deb0c6815eb
p = 0x00c2bd20b6533624a260e003e48f9b033f259f7f73f7612205a550dd8987326aa3d491286b98929ea942e616686273fa8d3f57a741c426b34bb918b2608a66f0f76e4af572c35aa72b73763fa5fb17fa702852c1f008f40c1685bc404942a508548f9361716938191204a425ed1c0ee20ca8e671cb2634246cdf60fbd6cc9ba703
q = 0x00c1394bc9a82d99d3955965724e0d3f141c0056099b2c6061bd721dd8fe1a80e7fe78273c532b4e24737c874d327dc05b413925e37bbf6321e0b649d52b0be4033af616bdbe7a64334b336597a30da66cc583e28212d596f4f4a2ad58de12709e14a28a1bfdbf9cc40756ffd2783edc311d7d3be1eed22c83ccecc5003d658871
c = 0x20b77e4dbd5cef24e5bf0388ba467e247e1da2963e521bd9c577c5237a2b7b53f7b5755e95880595286f00b85237e3628ac27360d7e4cdd69125511c15db5ab31f37a8f6d81909a0b62a3f8d6083a61289801d60730a260658b3d12d521bfdb5977e2b05d5db42db5dff9c2192512a3834992a57c95bb88b4f863533810609fb9da993441e3641d46cb97ce7387dccae6f7802000252a81b7a2b156eb2d7da4dd2b920c11c0a2979f3ae5f44289a5c529bc6417d492faa52c0f13e0c3dda4f5cfdd63fd66f0fac79dfe635fcec7ce53843dc5c3c81587558a43cb0744b2c3dbb4b3d17a53be0c628c497455bfb2bb3e297a129007e5a648ca18d46521e43ea6a

pri = RSA.construct((n,e,d,p,q))
bytes.fromhex(hex(pri.decrypt(c))[2:])
```

This outputs the following:

```
b'Now that we figured out how to properly encrypt our messages, we can finally safely share our secret: flag{Hastad_broadcast_attack}'
```

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Bad Guy Message Flag
> ```
> flag{Hastad_broadcast_attack}
> ```

</details>
