# Re-Broadcast (250 Points)

## Description

Oh no! The boss found out that badguys 1 through 3 followed an incorrect guide to create their RSA keypair. Those 3 have been sacked, and replaced with badguys 4 through 6. Now we'll have to break their encryption as it was originally intended!

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* python
  * pycryptodome
  * sympy

### Walkthrough

For this challenge, we are given a zip file with 3 public RSA keys and 3 ciphertext messages. The messages are all the same plaintext.

I inspected the keys with the following command:

> ```
> openssl rsa -pubin -inform PEM -text -noout -in badguy4.pub
> ```
>
> ```
> RSA Public-Key: (2048 bit)
> Modulus:
>     00:af:0c:ad:09:e3:a4:d5:37:89:b8:5e:07:6a:b2:
>     5d:9a:fe:66:47:60:31:95:01:f6:dd:ee:8f:26:7d:
>     62:85:53:43:ce:2e:84:f0:f0:b9:aa:c8:d3:a0:30:
>     8c:0a:07:79:cb:06:b6:f3:bf:bb:4f:66:68:37:6f:
>     d6:14:51:e0:a0:42:56:03:c5:7e:db:48:0d:54:ec:
>     f6:49:f5:cb:66:3a:de:a0:88:e9:3a:27:f1:6b:d6:
>     5f:18:a5:fb:5a:f5:88:80:83:4f:d7:43:df:c1:7c:
>     17:28:c3:60:29:60:b4:a3:eb:28:87:37:ae:05:bf:
>     51:b5:16:5d:10:93:ce:23:38:ef:2a:d1:ab:a3:12:
>     68:dd:e1:1a:5b:9b:b3:c2:60:c4:23:b2:ed:23:65:
>     a6:7e:56:f7:21:6a:3f:85:bc:0b:3e:e7:ee:f2:47:
>     b9:31:e3:3f:48:8a:f3:dd:b0:e0:4d:b8:66:94:ff:
>     5e:30:ce:8b:23:63:24:3c:b8:2c:ae:8b:ca:36:95:
>     aa:32:f1:24:37:10:17:05:15:c3:7e:57:ad:cd:cc:
>     c3:79:8f:c9:5e:7f:a6:56:30:97:09:57:27:9a:05:
>     03:b1:16:90:b5:ba:6d:32:5c:fa:fc:2a:b2:af:6f:
>     d8:3f:a8:11:f6:98:8a:80:b6:ce:b0:69:6e:84:78:
>     3a:f3
> Exponent: 3 (0x3)
> ```
>
> ```
> RSA Public-Key: (2048 bit)
> Modulus:
>     00:c4:f8:6e:60:1a:d7:44:7c:d5:d8:e4:2c:f5:99:
>     03:41:19:b8:84:e0:4c:a7:53:81:6a:0b:de:5a:39:
>     49:2f:3e:79:8d:0c:ec:3d:bc:fb:81:3e:bc:95:ad:
>     2b:94:ad:0b:33:7e:11:51:c8:f9:b3:52:6f:9f:67:
>     e0:d2:64:66:c4:61:1b:3a:56:1b:21:76:d0:b0:d9:
>     89:7a:1b:0c:86:41:2a:22:a6:16:26:9d:37:24:9f:
>     04:38:00:22:db:b7:67:b7:67:19:60:f4:02:57:99:
>     9f:f0:66:46:2a:7c:d8:c3:56:04:52:56:0d:c5:fe:
>     ed:bf:f2:ce:29:12:51:54:0e:50:e9:f4:7d:37:ff:
>     49:ad:86:46:43:4b:14:23:cb:9e:04:ce:62:4e:5f:
>     81:5e:f9:4f:3d:55:d8:2c:6a:50:7c:6d:15:f0:bf:
>     64:c7:79:b8:25:d7:ac:39:8a:02:9c:6f:8c:84:5c:
>     bf:20:c5:f6:1a:3a:16:4d:7b:77:a0:7b:52:46:e3:
>     28:c2:86:5d:60:38:64:a6:e6:9f:b8:fa:45:f9:80:
>     8e:d0:e5:ad:60:4c:f1:6d:5b:a9:7c:37:00:b6:2d:
>     49:1b:23:eb:17:80:6e:9f:5e:3a:10:e9:1c:5d:16:
>     01:26:0b:8b:05:de:d0:d3:2c:94:4c:88:92:20:bd:
>     da:d7
> Exponent: 3 (0x3)
> ```
>
> ```
> RSA Public-Key: (2048 bit)
> Modulus:
>     00:c4:d4:4a:36:25:7b:da:9d:cd:4c:cd:0b:1c:d8:
>     60:ce:36:12:95:5b:cf:01:a1:1a:2f:55:90:6e:c5:
>     d8:94:78:1e:05:be:72:38:4e:0e:a8:8d:c0:cd:b4:
>     17:f8:05:cd:f0:2b:c6:97:59:1d:5d:56:97:f5:58:
>     e6:3c:c0:d5:9a:84:c9:45:10:9c:57:02:10:df:97:
>     34:be:6d:1d:8d:8d:ba:6e:5f:9d:e8:7f:a0:83:c4:
>     97:b6:4c:a8:c2:90:ea:aa:31:36:66:26:a3:0e:eb:
>     2c:f7:48:a8:8b:77:33:d8:66:a9:1b:b9:53:68:9d:
>     2f:6f:c7:ac:9e:73:47:28:08:d6:8e:fe:a5:1b:c1:
>     f5:ba:d5:88:86:cb:a3:3c:24:bb:3f:71:e5:b2:53:
>     a6:d6:89:4f:ba:d5:b8:e2:d7:8d:84:cb:1e:83:2d:
>     09:20:a3:da:f2:99:7d:74:e5:30:de:49:b1:54:30:
>     13:16:df:6e:1b:df:00:f6:fc:ab:8d:8e:63:40:8d:
>     b4:81:9f:5b:08:1d:4c:f6:2c:db:d7:68:6a:b7:0d:
>     88:b9:ca:61:a6:74:6f:6d:cb:7d:99:8b:a0:59:1f:
>     26:5e:43:8f:4d:d0:92:ab:b3:c3:4c:4d:b1:05:6e:
>     43:00:b1:e0:d1:4a:91:56:5d:89:ed:49:5b:bd:a3:
>     59:a9
> Exponent: 3 (0x3)
> ```

As we can see, the three Exponents are all 3. I did some googling and found [this](https://www.johndcook.com/blog/2019/03/06/rsa-exponent-3/) article, which details an attack that can be made with this type of setup.

I then made the following Python script:

> ```
> #!/usr/bin/env python3
> 
> from Crypto.PublicKey import RSA
> from sympy.ntheory.modular import crt
> from sympy import cbrt
> 
> N = []
> C = []
> 
> for i in range(4,7):
>     with open(f"badguy{i}.pub") as f:
>         N.append(RSA.importKey(f.read()).n)
>     with open(f"badguy{i}.enc", 'rb') as f:
>         C.append(int(f.read().hex(), 16))
> 
> m = cbrt(crt(N, C)[0])
> 
> print(bytes.fromhex(hex(m)[2:]).decode())
> ```

Note that you will need to `pip3 install sympy pycryptodome` if you don't already have them.

This iterates through our 3 bad guys and pulls out the Modulus (N) and the ciphertext and stores them in lists. I then ran these lists through the `crt` and `cbrt` functions mentioned in the article I linked earlier and was given our decoded message. The only problem is that it was returned as an integer. The final line converts it into hex, then a bytestring, then ascii.

We are then given the following text:

> Okay, so badguys 1 through 3 have been sacked because they though it was a good idea to have their private keys in their public keys. flag{I'm_not_going_to_repeat_myself_more_than_3_times_because_that_would_be_unsafe}

</details>

## Flags

<details>
<summary>Show Flag</summary>

> ```
> flag{I'm_not_going_to_repeat_myself_more_than_3_times_because_that_would_be_unsafe}
> ```

</details>
