# Look Closer

## Description

You are a potential new hire for your organization’s forensics department. While your resume is impeccable and you impressed your new supervisor during the interview, you still have one challenge left to overcome. Your supervisor to be has provided you with 9 image files to analyze. You must find which of these files have been manipulated, how, and what information these altered files contain. If you pass, you are on the forensics team with a nice pay increase. What was the make/manufacturer of the camera these pictures were taken with? Submit just the brand name.

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* exif

### Walkthrough

Those familiar with photo metadata should know the command exif. It prints this information.

Running `exif 1.png` gave me the following information:

```
XIF tags in '1.jpg' ('Intel' byte order):
--------------------+----------------------------------------------------------
Tag                 |Value
--------------------+----------------------------------------------------------
Image Description   |
Manufacturer        |NIKON
Model               |COOLPIX P6000
Orientation         |Top-left
X-Resolution        |300
Y-Resolution        |300
Resolution Unit     |Inch
Software            |Nikon Transfer 1.1 W
Date and Time       |2008:11:01 21:15:11
YCbCr Positioning   |Centered
Compression         |JPEG compression
X-Resolution        |72
Y-Resolution        |72
Resolution Unit     |Inch
Exposure Time       |1/131 sec.
F-Number            |f/4.1
Exposure Program    |Normal program
ISO Speed Ratings   |64
Exif Version        |Exif Version 2.2
Date and Time (Origi|2008:10:22 16:55:37
Date and Time (Digit|2008:10:22 16:55:37
Components Configura|Y Cb Cr -
Exposure Bias       |0.00 EV
Maximum Aperture Val|2.90 EV (f/2.7)
Metering Mode       |Pattern
Light Source        |Unknown
Flash               |Flash did not fire, compulsory flash mode
Focal Length        |6.0 mm
Maker Note          |3298 bytes undefined data
User Comment        |
FlashPixVersion     |FlashPix Version 1.0
Color Space         |sRGB
Pixel X Dimension   |640
Pixel Y Dimension   |480
File Source         |DSC
Scene Type          |Directly photographed
Custom Rendered     |Normal process
Exposure Mode       |Auto exposure
White Balance       |Auto white balance
Digital Zoom Ratio  |0.00
Focal Length in 35mm|28
Scene Capture Type  |Standard
Gain Control        |Normal
Contrast            |Normal
Saturation          |Normal
Sharpness           |Normal
Subject Distance Ran|Unknown
North or South Latit|N
Latitude            |43, 27, 57.6419999
East or West Longitu|E
Longitude           |11, 52, 44.8019999
Altitude Reference  |Sea level
GPS Time (Atomic Clo|14:54:00.19
GPS Satellites      |04
GPS Image Direction |
Geodetic Survey Data|WGS-84
GPS Date            |2008:10:23
Interoperability Ind|R98
Interoperability Ver|0100
--------------------+----------------------------------------------------------
EXIF data contains a thumbnail (5159 bytes).
```

The most important line here is of course:

```
Manufacturer        |NIKON
```

</details>

## Flags

<details>
<summary>Show Flag</summary>

> What was the make/manufacturer of the camera these pictures were taken with? Submit just the brand name.
> ```
> NIKON
> ```

</details>
