# Look Closer 3

## Description

What are the full filenames of the 2 files that were altered some way? Enter it in alphabetical/numerical order with no space: a.ext,b.ext

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* exif

### Walkthrough

For this one, I just looked at the exif data for differences in the byte count with commands like the following:

```
exif 1.jpg | grep byte
```

Most returned the following:

```
EXIF tags in '1.jpg' ('Intel' byte order):
Maker Note          |3298 bytes undefined data
EXIF data contains a thumbnail (5159 bytes).
```

`4.jpg` returned the following message:

```
Corrupt data
The data provided does not follow the specification.
ExifLoader: The data supplied does not seem to contain EXIF data.
```

`7.jpg` returned the following:

```
EXIF tags in '7.jpg' ('Intel' byte order):
Maker Note          |3394 bytes undefined data
EXIF data contains a thumbnail (8480 bytes).
```

Therefore, our flag is `4.jpg,7.jpg`.

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Altered Files
> ```
> 4.jpg,7.jpg
> ```

</details>
