# Look Closer 4

## Description

What is the single-word, alphanumeric string found when viewing the image of one of the altered files?

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* [stegsolve](http://www.caesum.com/handbook/stego.htm) ([Download](http://www.caesum.com/handbook/Stegsolve.jar))

### Walkthrough

`Stegsolve` was the tool to use here. Basically, it is a java tool that lets you click through all sorts of filters to find hidden words. It also has a handful of other really neat features, but that's the one we're using here.

Loading up `4.jpg` and clicking through showed us the following text, but it's not a single-word, alphanumeric string, so I kept looking:

![](../../images/look_closer_4_wrong.bmp)

Loading up `7.jpg` and clicking through showed us `N3v3r`, which is a single-word, alphanumeric string, and our flag!

![](../../images/look_closer_4_right.bmp)

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Single-word, Alphanumeric String
> ```
> N3v3r
> ```

</details>
