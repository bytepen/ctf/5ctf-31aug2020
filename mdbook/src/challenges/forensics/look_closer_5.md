## Look Closer 5

## Description

What tool is used to uncover the hidden file in the second altered image?

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* Luck

### Walkthrough

If I'm totally honest, this one was a guess. `steghide` is a common program to extract/hide hidden files from/in images.

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Tool Used to uncover the hidden file
> ```
> steghide
> ```

</details>
