# Look Closer 6

## Description

What is the final flag found in the hidden file?

Stuck? Maybe "john" can help you.

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* steghide
* [rsmangler](https://github.com/digininja/RSMangler)
* [steghide-crack](https://github.com/felipesi/steghide-crack)

### Walkthrough

Not gonna lie, this one took me WAY longer than it should have. I figured that the file was probably hidden inside of `4.jpg` because `Look Closer 5` mentioned the `second file` and previous challenges used `7.jpg`. I tried all the different passwords that I could think of at least a few times, then started brute forcing things. I found the `steghide-crack` script mentioned above and passed it `rockyou` at first. I had no luck with that, so I started trying `rsmangler`. Eventually I  tried to run `N3v3r` through rsmangler and one of the passwords it generated was `n3v3r`. `steghide-crack` found this password immediately. It saved a file called `output.txt` that had the text `So close!` inside of it. Obviously I'm missing something here. I try `steghide extract -sf 4.jpg -p n3v3r` for myself and the file is named `ZmxhZ3tZMHVfRjB1bkRfbTN9`. While not super obvious, this is base64. I ran `echo ZmxhZ3tZMHVfRjB1bkRfbTN9 | base64 -d` and got `flag{Y0u_F0unD_m3}`

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Hidden File Flag
> ```
> flag{Y0u_F0unD_m3}
> ```

</details>
