# Memory

## Description

You are a potential new hire for your organization’s forensics department. While your resume is impeccable and you impressed your new supervisor during the interview, you still have one challenge left to overcome. Your supervisor to be has provided you with 1 .vmem file to analyze. You must find which operating system is associated with this file and 4 additional flags hidden within the running processes, registry, and file system. If you pass, you are on the forensics team with a nice pay increase. Question 1: Which OS profile(s) is this memory file associated with? If multiple profiles, provide the answer such as: profile1,profile2,profile3

The file can be found at: https://drive.google.com/file/d/1wVzQ49KN9x5xNw_eg_O5PBkXrTLmVV2p/view?usp=sharing

## Solution

<details>
<summary>Show Solution</summary>

### Tools

</details>

## Flags

<details>
<summary>Show Flag</summary>

</details>
