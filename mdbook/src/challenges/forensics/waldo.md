# Waldo

## Description

The secret Hide Waldo organization recently had a data breach and Waldo's identity has been exposed.
While they relocate Waldo, they must confirm who and how his information was leaked.
The Network team picked up on suspicious traffic from an internal workstation (Wally's account) to an overseas IP.
Wally also happened to quit just days before the breach.
The team was able to save and carve out a suspicious looking file (TheTruth.txt) sent over unencrypted ftp which allowed for analysis.
Before we can bring this former employee's actions to justice, it must be confirmed that Wally was indeed the one who leaked the top secret information and inevitably lead to the chaos of 2020.

Intel: Wally has been known to reuse passwords despite company policy.

First question: What is the password(s) used to encrypt the file(s)?

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* fcrackzip
* rockyou.txt

### Walkthrough

I downloaded `TheTruth.txt` and ran `file TheTruth.txt`.
This revealed that this was actually `Zip archive data`.
I renamed `TheTruth.txt` to `TheTruth.zip` and ran `unzip TheTruth.zip`.
It asked for a passsword.

With the hint about password reuse, I felt the pretty obvious assumption was that Wally has terrible password hygiene and that his password would be in `rockyou`.

I ran `fcrackzip -D -u -p ./rockyou.txt` and quickly found out that the password was `pimeapple`. (That is not a typo)

I then ran `unzip TheTruth.zip` again and input the password `pimeapple`.
`DoNotProceed.rtf` popped out, but we will contiune with that in the next challenge.

</details>

## Flags

<details>
<summary>Show Flag</summary>

> What is the password?
> ```
> pimeapple
> ```
> This is with an m as in Mary. It is not a typo.

</details>
