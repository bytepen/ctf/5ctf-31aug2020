# Waldo 2

## Description

What is the hidden message that can prove Wally's guilt?

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* file
* various decompression tools
* xxd
* steghide

### Walkthrough

Picking up where we left of in the last challenge, we have `DoNotProceed.rtf`.
Running `file` on this will tell you it is `ASCII`.
Reading it will dump something like this:

```
00000000: 425a 6839 3141 5926 5359 6d19 5862 00d8  .!........_....Q
00000010: 7dff ffff ffff ffff ffff ffff ffff ffff  '...............
00000020: ffff ffff ffff ffff ffff ffff ffff ffff  ................
00000030: ffff ffe8 ca0a 38a2 dece ba0f 5776 e683  ...Y...s......Wc
00000040: a5da dab7 bcdc 741e b763 d9e7 24f6 6f54  v.........RX.6?.
00000050: ef63 d3b6 17b3 2551 e879 49db 3a6e c3d0  ..L.....Y`...>C}
00000060: 8ef7 0eed eb5d d9ea ddc7 6ee0 7a07 5ecf  .7...)R..G>\:.;.
00000070: 72f5 a8f7 7bbd 6d6b b677 bde2 9e81 d2ba  .5y7#]_,..]S.aK.
00000080: 683d daf6 3bd9 a509 af4e 6767 2ddb d6dd  ...6.Rv..+....O.
00000090: 3de7 a3d1 a3d6 88ee b9dd 9e9e 973d c1e8  .XtJtOh.....p.AY
...
```

You may recognize this as the output of `xxd`.
What you may not know is that `xxd` can also put this back into its original file easily.
I ran `cat DoNotProceed.rtf | xxd -r > DoNotProceed`.

I ran `file DoNotProceeed` and it told me it was `bzip2 compressed data`.
I ran `bunzip2 DoNotProceed` and `DoNotProceed.out` popped out.

I ran `file DoNotProceed.out` and it told me it was `gzip compressed data`.
I renamed it to `DoNotProceed.gz` and ran `gunzip DoNotProceed.gz`.
Out poppped `DoNotProceed`.

I ran `file DoNotProceed` and it told me it was `XZ compressed data`.
I renamed it to `DoNotProceed.xz` and ran `xz -d DoNotProceed.xz`.
`DoNotProceed` popped out.

I ran `file DoNotProceed` and it told me it was `POSIX tar archive (GNU)`.
I ran `tar xvf DoNotProceed` and `findme.mov` popped out.

I ran `file findme.mov` and it told me it was `JPEG image data`.
I renamed it to `findme.jpg`.
I spent the time to find Waldo and confirmed that Wally has been sending out images of Waldo.
Does it matter? Nope! Back to work!

I remembered the hint from the last challenge about password reuse and attempted `steghide extract -sf findme.jpg`.
I entered the password `pimeapple` (not a typo) and out popped `flaginside.txt`.
This file was actually a `txt` file for once and contained `flag{TheRealWaldoisWally}`.

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Hidden Message
> ```
> flag{TheRealWaldoisWally}
> ```

</details>
