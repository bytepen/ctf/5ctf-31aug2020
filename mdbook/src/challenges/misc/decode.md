# Decode (80 Points)

## Description

You have discovered the following encrypted message and need to find a way to see what is going on.

Submit the entire decoded message as the flag.

```
bW9yZSBjb2RlIDAxMDAwMTAwIDAxMTAwMTAxIDAxMTAwMDExIDAxMTAxMTExIDAxMTAwMTAwIDAxMTAwMTAxIDAwMTAwMDAwIDAxMDEwMTExIDAxMDEwMTExIDAwMTExMDAxIDAwMTEwMDAxIDAxMDAxMDAxIDAxMDAwMTExIDAxMTAxMDAwIDAxMTAxMDAwIDAxMTAwMTAwIDAxMTAxMTAxIDAxMDEwMTAxIDAxMTAwMTExIDAxMTAwMDExIDAwMTEwMDExIDAxMDEwMTEwIDAxMTAxMDEwIDAxMDExMDAxIDAwMTEwMDEwIDAxMDEwMTEwIDAxMTExMDEwIDAxMTAwMDExIDAwMTEwMDEwIDAxMDExMDEwIDAwMTEwMDAxIDAxMTAwMDEwIDAxMDAwMTExIDAxMTExMDAwIDAwMTEwMTAxIDAxMDAxMDAxIDAxMDAwMTExIDAxMTAwMTAwIDAxMTEwMTEwIDAxMTAwMTAwIDAxMDAwMDExIDAxMDAwMDEwIDAxMTEwMTAwIDAxMTAwMTAxIDAxMDEwMDExIDAxMDAwMDEwIDAxMTEwMTAwIDAxMDExMDEwIDAxMDExMDAwIDAxMDAxMTEwIDAxMTExMDEwIDAxMDExMDAxIDAxMDEwMTExIDAxMTAwMTAwIDAxMTAxMTAwIDAxMDAxMTAwIDAxMTAxMDAxIDAxMDAwMDAxIDAxMTAwMTExIDAxMDEwMDExIDAxMDEwMDExIDAxMDAwMDEwIDAxMTEwMTAxIDAxMDExMDEwIDAxMDEwMTExIDAxMDEwMTEwIDAxMTAxMDExIDAxMDAxMDAxIDAxMDAxMDAwIDAxMTAxMTAwIDAxMTEwMTEwIDAxMTAwMTAwIDAxMDEwMDExIDAxMDAwMDEwIDAwMTEwMDAwIDAxMTAwMDEwIDAxMTExMDAxIDAxMDAwMDEwIDAxMTAxMDAxIDAxMDExMDEwIDAxMDEwMTExIDAxMTAwMTAwIDAxMTEwMDAwIDAxMTAwMDEwIDAxMTAxMDAxIDAxMDAwMDEwIDAwMTEwMDAwIDAxMTAwMDExIDAxMTAxMTAxIDAxMDAwMTEwIDAxMTEwMTAxIDAxMTAwMDExIDAwMTEwMDEwIDAxMDExMDEwIDAxMTAxMTAwIDAxMTAwMDExIDAxMTAxMTEwIDAxMDAxMDEwIDAxMTEwMDAwIDAxMTAwMDEwIDAxMTAxMTAxIDAxMTAwMDExIDAxMTAwMTExIDAxMDExMDAxIDAxMDEwMTExIDAxMTExMDAwIDAxMTEwMDExIDAxMDAxMDAxIDAxMDAxMDAwIDAxMDAxMTEwIDAwMTEwMDAwIDAxMTAwMDEwIDAwMTEwMDEwIDAxMTExMDAwIDAxMTAxMTAwIDAxMTAwMDEwIDAxMTAxMDAxIDAxMDAwMDEwIDAxMTAxMDExIDAxMDExMDAxIDAxMDExMDAwIDAxMDEwMDEwIDAxMTAxMDAwIDAxMDAxMDAxIDAxMDAxMDAwIDAxMDEwMDEwIDAxMTEwMTEwIDAxMDAxMDAxIDAxMDAxMDAwIDAxMDEwMDEwIDAxMTAxMTExIDAxMDExMDEwIDAxMDEwMDExIDAxMDAwMDEwIDAxMTEwMTExIDAxMTAwMDExIDAxMTAxMTAxIDAxMDEwMTEwIDAxMTAxMDExIDAxMDExMDEwIDAxMDExMDAwIDAxMDEwMDEwIDAxMTAxMTAwIDAxMTAwMDExIDAxMTAxMTAxIDAwMTEwMDAxIDAxMTEwMDAwIDAxMTAwMDEwIDAxMTAxMTAxIDAxMDEwMTEwIDAxMTAxMDExIDAxMDAxMDAxIDAxMDAwMTExIDAxMDEwMDEwIDAxMTExMDAxIDAxMTAwMDEwIDAwMTEwMDExIDAxMDAwMDAxIDAxMTAwMTExIDAxMTAwMDExIDAwMTEwMDEwIDAxMTAxMTAwIDAwMTEwMDAwIDAxMDExMDEwIDAxMDEwMDExIDAwMTEwMTAwIDAwMTExMTAxCg==
```

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* CyberChef

### Walkthrough

Most people will recognize this right off the bat as base64 due to the `==` at the end. I dropped this into CyberChef and selected `from base64`. This output the following:

```
more code 01000100 01100101 01100011 01101111 01100100 01100101 00100000 01010111 01010111 00111001 00110001 01001001 01000111 01101000 01101000 01100100 01101101 01010101 01100111 01100011 00110011 01010110 01101010 01011001 00110010 01010110 01111010 01100011 00110010 01011010 00110001 01100010 01000111 01111000 00110101 01001001 01000111 01100100 01110110 01100100 01000011 01000010 01110100 01100101 01010011 01000010 01110100 01011010 01011000 01001110 01111010 01011001 01010111 01100100 01101100 01001100 01101001 01000001 01100111 01010011 01010011 01000010 01110101 01011010 01010111 01010110 01101011 01001001 01001000 01101100 01110110 01100100 01010011 01000010 00110000 01100010 01111001 01000010 01101001 01011010 01010111 01100100 01110000 01100010 01101001 01000010 00110000 01100011 01101101 01000110 01110101 01100011 00110010 01011010 01101100 01100011 01101110 01001010 01110000 01100010 01101101 01100011 01100111 01011001 01010111 01111000 01110011 01001001 01001000 01001110 00110000 01100010 00110010 01111000 01101100 01100010 01101001 01000010 01101011 01011001 01011000 01010010 01101000 01001001 01001000 01010010 01110110 01001001 01001000 01010010 01101111 01011010 01010011 01000010 01110111 01100011 01101101 01010110 01101011 01011010 01011000 01010010 01101100 01100011 01101101 00110001 01110000 01100010 01101101 01010110 01101011 01001001 01000111 01010010 01111001 01100010 00110011 01000001 01100111 01100011 00110010 01101100 00110000 01011010 01010011 00110100 00111101
```

Most people will recognize this as binary pretty quick.

I removed `more code` and pasted it into CyberChef, then selected `From Binary`. This output the following:

```
Decode WW91IGhhdmUgc3VjY2Vzc2Z1bGx5IGdvdCBteSBtZXNzYWdlLiAgSSBuZWVkIHlvdSB0byBiZWdpbiB0cmFuc2ZlcnJpbmcgYWxsIHN0b2xlbiBkYXRhIHRvIHRoZSBwcmVkZXRlcm1pbmVkIGRyb3Agc2l0ZS4=
```

base64 again. Decode with CyberChef to get:

```
You have successfully got my message.  I need you to begin transferring all stolen data to the predetermined drop site.
```

There we have our flag.

</details>

## Flags

<details>
<summary>Show Flag</summary>

> The entire decoded message
> ```
> You have successfully got my message.  I need you to begin transferring all stolen data to the predetermined drop site.
> ```

</details>
