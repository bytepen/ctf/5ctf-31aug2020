# Git Gone (120 Points)

## Description

What is the commit hash if you remove Im_JuSt_A_fIlE.tXt from the git entirely?

(remove it from ALL commits)

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* git

### Walkthrough

Typically the whole idea around git is that every change you've ever made is right there are your fingertips, waiting for you to need to go back to it.
That's handy, but sometimes you need to purge a file entirely from the repository.
This is actually good real-world practice in case you ever screw up and upload some super sensitive data to a git repository.
There's a couple ways to do this, but I've always liked `bfg` so long as Java is willing to cooperate.

You have to download the `bfg.jar`, but then you can run the following command:

```
java -jar /mnt/c/Users/bamhm182/Downloads/bfg-1.13.0.jar --delete-files Im_JuSt_A_fIlE.tXt ./.git
```

Depending on a handful of factors, this can be super quick or take a very long time.
Here it is quick.
If we do a `git log`, we can see that our new commit hash is `04635da2cc0dc92d6905aa948cc8eae8de62e7e9`.


### Alternative Solution

What's really interesting is that since there are a few different ways to do this, there are also a few different hashes you can wind up with.
Another solution is to use the built in `git filter-branch` command as is shown below:

```
git filter-branch --force --index-filter           \
    'git rm --cached --ignore-unmatch Im_JuSt_A_fIlE.tXt'        \
    --prune-empty --tag-name-filter cat -- --all
```

This one gives us the hash `c3c403938694f9bd76841e6237ab2b9020f1ce85`.

One note is that this command will also delete the commits from `git log`, whereas `bfg` will leave them.
It depends on your goal, but one may be more desirable than the other in certain circumstances.

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Commit Hash after Delete
> ```
> 04635da2cc0dc92d6905aa948cc8eae8de62e7e9
> ```

> Alternative Commit Hash after Delete
> ```
> c3c403938694f9bd76841e6237ab2b9020f1ce85
> ```

</details>
