# Git Gud 0 (10 Points)

## Description

What is the commit hash of master?

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* Git

### Walkthrough

I love git, so I was pretty excited to see some git challenges.
Definitely never used git in some of the ways it was used in these challenges, though!

For this series of challenges, we are given a single `.zip` file that we will be using across them all.
We start off by unzipping it, then we can drop into terminal.

The way I chose to answer this challenge was to execute the command `git log`.
This command returns a "history" of times that the git repository was updated via a "commit".
For example, these are the first 3 entries:

```
commit 31f1ae3d4fad39e179bf3bb2e1d2ca65e46e19a6 (HEAD -> master)
Author: WhiteCell <WhiteCell@5charlie.com>
Date:   Tue Aug 4 14:33:15 2020 -0500

    Challenge starts here

commit 0f6e5c6ac494d6b71071985166a3dce419f406b0
Author: Rip <WhiteCell@5charlie.com>
Date:   Tue Aug 4 14:33:15 2020 -0500

    Change challenge text

commit bc3ac96a01d0b45044aca96c3ff3b1c2594c2e4b
Author: Cale <WhiteCell@5charlie.com>
Date:   Tue Aug 4 14:33:15 2020 -0500

    Change challenge text
```

The current commit hash of the master branch is:

```
31f1ae3d4fad39e179bf3bb2e1d2ca65e46e19a6
```

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Master Commit Hash
> ```
> 31f1ae3d4fad39e179bf3bb2e1d2ca65e46e19a6
> ```

</details>
