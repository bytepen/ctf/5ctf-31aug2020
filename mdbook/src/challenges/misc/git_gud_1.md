# Git Gud 1 (50 Points)

## Description

How many commits have been made to this repository?

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* Git

### Walkthrough

This one was a bit tricky for me. I usually just look on the GitLab/GitHub web UI if I want to know.
That said, it isn't hard and can be done with the following command:

```
git rev-list --all --count
```

This returns `302`.

</details>

## Flags

<details>
<summary>Show Flag</summary>

> How Many Commits?
> ```
> 302
> ```

</details>
