# Git Gud 2 (50 Points)

## Description

Which username authored the most commits?

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* Git

### Walkthrough

This was also a command I was unfamiliar with.
Running the following command will return a list of users and how many commits they've each made:

```
git shortlog -sne
```

The top few entries of this list are as follows:

```
    26  Haleigh <WhiteCell@5charlie.com>
    22  Eduard <WhiteCell@5charlie.com>
    18  Emmy <WhiteCell@5charlie.com>
    18  Matty <WhiteCell@5charlie.com>
    17  Elijah <WhiteCell@5charlie.com>
    17  Hurlee <WhiteCell@5charlie.com>
    17  Llewellyn <WhiteCell@5charlie.com>
```

We can see that `Haleigh` authored the most commits.

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Who authored the most commits?
> ```
> Haleigh
> ```

</details>
