# Git Gud 3 (50 Points)

## Description

What file is was removed from the git repository prior to the current commit?

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* Git

### Walkthrough

If we do an `ls`, we can see there is only one file titled `hint.txt` in this repository that has the contents `This isn't the file you're looking for`. Much Jedi.

If we recall the `git log` command from earlier, we can get the `commit hash` of the previous commit.

```
commit 31f1ae3d4fad39e179bf3bb2e1d2ca65e46e19a6 (HEAD -> master)
Author: WhiteCell <WhiteCell@5charlie.com>
Date:   Tue Aug 4 14:33:15 2020 -0500

    Challenge starts here

commit 0f6e5c6ac494d6b71071985166a3dce419f406b0
Author: Rip <WhiteCell@5charlie.com>
Date:   Tue Aug 4 14:33:15 2020 -0500

    Change challenge text
```

Here we can see the previous commit was `0f6e5c6ac494d6b71071985166a3dce419f406b0`.
With that, we can use the command `git checkout 0f6e5c6ac494d6b71071985166a3dce419f406b0` to switch to the previous commit. Now we see a file titled `Im_JuSt_A_fIlE.tXt`.
This is our flag.

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Deleted File?
> ```
> Im_JuSt_A_fIlE.tXt
> ```

</details>
