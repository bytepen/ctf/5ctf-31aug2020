# Git Gud 4 (50 Points)

## Description

Before it was removed, what was the md5sum of Im_JuSt_A_fIlE.tXt?

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* md5sum

### Walkthrough

This is less of a `git` question than the others, but it's easy enough, just run the following command:

```
md5sum Im_JuSt_A_fIlE.tXt
```

This returns the following:

```
ebf06dc84c65f37d3bc51e3a2294274d  Im_JuSt_A_fIlE.tXt
```

This tells us the md5sum is `ebf06dc84c65f37d3bc51e3a2294274d`.

</details>

## Flags

<details>
<summary>Show Flag</summary>

> What is the md5sum of Im_JuSt_A_fIlE.tXt?
> ```
> ebf06dc84c65f37d3bc51e3a2294274d
> ```

</details>
