# Git Gud 5 (100 Points)

## Description

It looks like someone added 5charlie into Im_JuSt_A_fIlE.tXt as a base64 string. What is the commit hash immediately before the first time 5charlie was added?

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* git

### Walkthrough

This one was a little bit tricky and I ended up with a gross bash oneliner.
There's probably a better way to do this.

```
for i in {300..200}; do echo $i; git diff HEAD~$i Im_JuSt_A_fIlE.tXt | egrep '^\-{1}[A-z]' | tr -d "-" | base64 -d | grep 5charlie; done
```

From a previous challenge, we knew there were 302 commits.
We can leverage a trick that we haven't used in the previous challenges to make this a little easier.
That trick is that you can reference a previous commit with the `HEAD~#` format.
For example, `HEAD~1` refers to the previous commit, `HEAD~5` refers to 5 commits back.
I used `for i in {300..200}` to iterate through all of the previous branches one at a time, starting with the oldest.

We can use use the `git diff` command to check the difference two commits for a given file. I used a combination of `egrep` and `tr` to rip out the contents of the file from the specific commit we are looking at, then used base64 to decode it. If it contained `5charlie`, it would print the line.

After a few seconds, we see the following output:

```
...
225
224
223
222
reengage heartfully 5charlie unurged Rizal cogue menge skipjacks facebooked
221
derision angiotensinogen begemmed rehydrates dabble srbija Hatshepsut lignone moonily unequitable wives facebooked Cassie partaken patty boondocks miting egotism uncomparably 5charlie
220
Rizal cogue 5charlie reengage heartfully menge unurged skipjacks
219
5charlie heartfully skipjacks suncare Rizal reengage cogue unurged menge
218
suncare heartfully reengage Rizal menge cogue facebooked unurged skipjacks 5charlie
...
```

This tells us that the first commit that added `5charlie` was `HEAD~222`, so the one right before that is `HEAD~223`.
We can then run `git checkout HEAD~223` and `git log` to see the following:

```
commit 40b2c7cc8a360c7757b590d215b6205ebaf8ba87 (HEAD)
Author: Clarance <WhiteCell@5charlie.com>
Date:   Tue Aug 4 14:33:13 2020 -0500

    Change challenge text
```

This gives us our flag `40b2c7cc8a360c7757b590d215b6205ebaf8ba87`.

</details>

## Flags

<details>
<summary>Show Flag</summary>

> What is the commit hash of the commit right before `5charlie` was added?
> ```
> 40b2c7cc8a360c7757b590d215b6205ebaf8ba87
> ```

</details>
