# Git Gud 6 (50 Points)

## Description

Which author put added the 5charlie base64?

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* git

### Walkthrough

If we're still on the same commit from the last challenge, this is a very easy challenge.
If not, we can get there by running `git checkout HEAD~223`.

Running `git log` here will show us the following output:

```
commit 40b2c7cc8a360c7757b590d215b6205ebaf8ba87 (HEAD)
Author: Clarance <WhiteCell@5charlie.com>
Date:   Tue Aug 4 14:33:13 2020 -0500

    Change challenge text

commit a9fd430f09ad75a0b00f45f64fa45888a13efe1a
Author: Jamison <WhiteCell@5charlie.com>
Date:   Tue Aug 4 14:33:13 2020 -0500

    Change challenge text
```

We can see here that the previous commit was made by `Jamison`.
The previous challenge had us figure out that this was the commit that `5charlie` was added, so `Jamison` is our culprit!

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Who added `5charlie`?
> ```
> Jamison
> ```

</details>
