# Misc

This category consisted of 17 challenges worth 1,010 Points.

I thought most of these were pretty good, but I'm not really sure why they were in the `misc` category since most seemed to fall in line with `programming`, while `Decode` seemed to fall in line with `Crypto`.
At any rate, I had fun and brushed up on some lesser known git commands.
