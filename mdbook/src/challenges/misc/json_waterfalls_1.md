# JSON_Waterfalls 1 (50 Points)

## Description

We have the results files from a previous CTF.

Who solved the most challenges?

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* python

### Walkthrough

This one is pretty easy if you're familiar with the `json` library of python. We just need to read the files in, then figure out who has completed the most challenges. I used the following code:

```
#!/usr/bin/env python3

import json
import operator

class JsonWaterfalls():
    def __init__(self):
        self.challenges = []
        self.users = []

    def read_files(self):
        with open('./challenges.json') as f:
            self.challenges = json.loads(f.read())
        with open('./users.json') as f:
            self.users = json.loads(f.read())

    @staticmethod
    def sort_d(dic):
        return sorted(dic.items(), key=operator.itemgetter(1), reverse=True)

    def q1(self):
        completed = {}

        for user in self.users:
            completed[user['username']] = len(user['solves'])
        return self.sort_d(completed)[0][0]


if __name__ == '__main__':
    jw = JsonWaterfalls()
    jw.read_files()
    print(f"1) Most Challenges Completed: {jw.q1()}")
```

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Who completed the most challenges?
> ```
> clethieulier31
> ```

</details>
