# JSON_Waterfalls 2 (50 Points)

## Description

Which user got the most points?

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* python

### Walkthrough

This one's super easy to solve if you already have the code from the last one. We just need to add the `q2()` function:

```
#!/usr/bin/env python3

import json
import operator

class JsonWaterfalls():
    def __init__(self):
        self.challenges = []
        self.users = []

    def read_files(self):
        with open('./challenges.json') as f:
            self.challenges = json.loads(f.read())
        with open('./users.json') as f:
            self.users = json.loads(f.read())

    @staticmethod
    def sort_d(dic):
        return sorted(dic.items(), key=operator.itemgetter(1), reverse=True)

    def q2(self):
        points = {}
        users = {}
        for challenge in self.challenges:
            points[challenge['id']] = challenge['points']
        for user in self.users:
            for solve in user['solves']:
                users[user['username']] = users.get(user['username'], 0) + points[solve['challenge_id']]
        return self.sort_d(users)[0][0]


if __name__ == '__main__':
    jw = JsonWaterfalls()
    jw.read_files()
    print(f"2) Most Points: {jw.q2()}")
```

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Most Points?
> ```
> ucomberbeach10
> ```

</details>
