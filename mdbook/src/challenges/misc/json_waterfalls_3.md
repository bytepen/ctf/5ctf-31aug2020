# JSON_Waterfalls 3 (50 Points)

## Description

Which how many points did ucomberbeach10 get?

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* python

### Walkthrough

For this one, I just changed the return of `q2()` so that it returns all the information about our first place user `ucomberbeach10`, then in the print statement, I specified which value I wished to output from `(username, points)`

```
#!/usr/bin/env python3

import json
import operator

class JsonWaterfalls():
    def __init__(self):
        self.challenges = []
        self.users = []

    def read_files(self):
        with open('./challenges.json') as f:
            self.challenges = json.loads(f.read())
        with open('./users.json') as f:
            self.users = json.loads(f.read())

    @staticmethod
    def sort_d(dic):
        return sorted(dic.items(), key=operator.itemgetter(1), reverse=True)

    def q2(self):
        points = {}
        users = {}
        for challenge in self.challenges:
            points[challenge['id']] = challenge['points']
        for user in self.users:
            for solve in user['solves']:
                users[user['username']] = users.get(user['username'], 0) + points[solve['challenge_id']]
        return self.sort_d(users)[0]


if __name__ == '__main__':
    jw = JsonWaterfalls()
    jw.read_files()
    print(f"2) Most Points: {jw.q2()[0]}")
    print(f"3) ucomberbeach10 Points: {jw.q2()[1]}")
```

</details>

## Flags

<details>
<summary>Show Flag</summary>

> ucomberbeach10 Points
> ```
> 13060
> ```

</details>
