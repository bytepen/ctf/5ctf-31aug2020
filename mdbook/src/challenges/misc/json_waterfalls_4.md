# JSON_Waterfalls 4 (50 Points)

## Description

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* python

### Walkthrough

This one isn't too crazy either. I first count the total number of solves from all users, then map that to the challenge id's and return the flag.

```
#!/usr/bin/env python3

import json
import operator

class JsonWaterfalls():
    def __init__(self):
        self.challenges = []
        self.users = []

    def read_files(self):
        with open('./challenges.json') as f:
            self.challenges = json.loads(f.read())
        with open('./users.json') as f:
            self.users = json.loads(f.read())

    @staticmethod
    def sort_d(dic):
        return sorted(dic.items(), key=operator.itemgetter(1), reverse=True)

    def q4(self):
        solves = {}
        for user in self.users:
            for solve in user['solves']:
                solves[solve['challenge_id']] = solves.get(solve['challenge_id'], 0) + 1
        least_solved = self.sort_d(solves)[-1][0]
        for challenge in self.challenges:
            if challenge['id'] == least_solved:
                return challenge['flag']


if __name__ == '__main__':
    jw = JsonWaterfalls()
    jw.read_files()
    print(f"4) Least Submitted Flag: {jw.q4()}")
```

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Least submitted flag
> ```
> flag{Tampflex}
> ```

</details>
