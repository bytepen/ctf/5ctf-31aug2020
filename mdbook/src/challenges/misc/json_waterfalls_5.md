# JSON_Waterfalls 5 (50 Points)

## Description

Who submitted the first flag?

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* python

### Walkthrough

For this one, I just set an impossibly high date and checked each solve for an earlier date. If it was earlier, the earliest date got updated along with the user who had that solve. This works because python can compare strings, so like `"1" < "9" == True` and `"A" < "Q" == True`.

```
#!/usr/bin/env python3

import json
import operator

class JsonWaterfalls():
    def __init__(self):
        self.challenges = []
        self.users = []

    def read_files(self):
        with open('./challenges.json') as f:
            self.challenges = json.loads(f.read())
        with open('./users.json') as f:
            self.users = json.loads(f.read())

    @staticmethod
    def sort_d(dic):
        return sorted(dic.items(), key=operator.itemgetter(1), reverse=True)

    def q5(self):
        earliest = '9999-99-99T99:99:99Z'
        earliest_user = ''
        for user in self.users:
            for solve in user['solves']:
                if solve['date'] < earliest:
                    earliest = solve['date']
                    earliest_user = user['username']
        return earliest_user


if __name__ == '__main__':
    jw = JsonWaterfalls()
    jw.read_files()
    print(f"5) First Solve: {jw.q5()}")
```

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Who submitted the first flag?
> ```
> tziebart2u
> ```

</details>
