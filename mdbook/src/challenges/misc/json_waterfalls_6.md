# JSON_Waterfalls 6 (100 Points)

## Description

Who got the most most bloods, and how many did they get? Format: user,#

Note: a blood is also known as a first-to-solve

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* python

### Walkthrough

We can add this definition to the code we've been working on:

```
#!/usr/bin/env python3

import json
import operator

class JsonWaterfalls():
    def __init__(self):
        self.challenges = []
        self.users = []

    def read_files(self):
        with open('./challenges.json') as f:
            self.challenges = json.loads(f.read())
        with open('./users.json') as f:
            self.users = json.loads(f.read())

    @staticmethod
    def sort_d(dic):
        return sorted(dic.items(), key=operator.itemgetter(1), reverse=True)

    def q6(self):
        bloods = {}
        for user in self.users:
            for solve in user['solves']:
                if bloods.get(solve['challenge_id']):
                    if solve['date'] < bloods[solve['challenge_id']][1]:
                            bloods[solve['challenge_id']] = [user['username'], solve['date']]
                else:
                    bloods[solve['challenge_id']] = [user['username'], solve['date']]
        blood_count = {}
        for blood in self.sort_d(bloods):
            blood_count[blood[1][0]] = blood_count.get(blood[1][0], 0) + 1
        winner = self.sort_d(blood_count)[0]
        return f"{winner[0]},{winner[1]}"

if __name__ == '__main__':
    jw = JsonWaterfalls()
    jw.read_files()
    print(f"6) Most First Bloods: {jw.q6()}")
```

This first sorts out who got the first blood for each challenge, then determines how many bloods each person got and returns the winner in the format user,#.

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Most Bloods
> ```
> tspellessy23,7
> ```

</details>
