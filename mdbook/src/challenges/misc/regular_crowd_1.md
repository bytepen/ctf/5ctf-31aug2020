# Regular Crowd 1 (50 Points)

## Description

Find the md5sum of the line that meets these criteria:

* Begins and ends with the same character.
* Has a double character (i.e. 33 or aa) that doesn't include the first or last character.

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* python

### Walkthrough

I solved this one with python.
It's ugly, but it got the job done.
I first set out to read in the file, then I created `q1()` to solve this challenge.
The code goes through every line in the file and checks that the first and last character match, then it iterates through all characters in the middle looking for duplicates.
I did it this way because the double character cannot be the first or the last character.
Once I found the correct line, I hashed it and returned it so I could print it.
This gave me `0144a8dd049f8c09c1e473979408aec6` after finding `VhMXXJccXyV`.

```
#!/usr/bin/env python3

import hashlib

class RegularCrowd():
    def __init__(self):
        self.lines = []

    def read_file(self):
        with open('regular_crowd.txt') as f:
            self.lines = f.read().splitlines()

    def q1(self):
        for line in self.lines:
            if line[0] == line[-1]:
                for i in range(1,len(line)-2):
                    if line[i] == line[i+1]:
                        return hashlib.md5(line.encode()).hexdigest()

if __name__ == '__main__':
    rc = RegularCrowd()
    rc.read_file()
    print(f"1) {rc.q1()}")
```

</details>

## Flags

<details>
<summary>Show Flag</summary>

> md5 of line that matched criteria
> ```
> 0144a8dd049f8c09c1e473979408aec6
> ```

</details>
