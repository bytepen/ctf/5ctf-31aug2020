# Regular Crowd 2 (50 Points)

## Description

Find the md5sum of the line that meets these criteria:

* 5 character palindrome

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* python

### Walkthrough

A palindrome is a word that is the same forward and backwards.
This challenge took a little bit more effort because it's looking for a 5 character palindrome, but all of the lines have more than 5 characters.
This means that the palindrome is going to be inside of the line.
I added the `q2()` function to the code from our last challenge.
In this function, I went through line by line, and then looked at each set of 5 characters within each line to see if it was the same forward and backwards.
When it was, I calculated the md5sum and returned it.
It returned `77471933d5f43d4f1bfce08d42aa7bb4` after finding `Wwptptp5Cpc` because `ptptp` == `ptptp`.

```
#!/usr/bin/env python3

import hashlib

class RegularCrowd():
    def __init__(self):
        self.lines = []

    def read_file(self):
        with open('regular_crowd.txt') as f:
            self.lines = f.read().splitlines()

    def q1(self):
        for line in self.lines:
            if line[0] == line[-1]:
                for i in range(1,len(line)-2):
                    if line[i] == line[i+1]:
                        return hashlib.md5(line.encode()).hexdigest()

    def q2(self):
        for line in self.lines:
            for i in range(len(line)-4):
                if line[i:i+5] == line[i+4:i-1:-1]:
                    return hashlib.md5(line.encode()).hexdigest()

if __name__ == '__main__':
    rc = RegularCrowd()
    rc.read_file()
    print(f"1) {rc.q1()}")
    print(f"2) {rc.q2()}")
```

</details>

## Flags

<details>
<summary>Show Flag</summary>

> md5 of line containing 5 character palindrome
> ```
> 77471933d5f43d4f1bfce08d42aa7bb4
> ```

</details>
