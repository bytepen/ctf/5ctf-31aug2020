# Discovery

## Description

In order to test basic auditing functionality, a sample of packet capture has been provided to analysts for review. Please answer the following using the provided packet capture from the server. What was the third unique webpage/domain to be requested?

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* Wireshark

### Walkthrough

This one's pretty easy once you know where to look. We know we're dealing with `HTTP(S)` websites from the question, so we navigate to `File` => `Export Objects` => `HTTP` and sort by `Packet`. Here, we can see which `Hostnames` have been visited in order and determine that `www.tripod.lycos.com` is the third as seen below:

![](../../images/discovery.png)

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Third Website
> ```
> www.tripod.lycos.com
> ```

</details>
