# Discovery 10

## Description

What was the web directory that 192.168.0.10 was hosting?

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* Wireshark

### Walkthrough

This picks back up from `Discovery 4`.

If we scroll down some, we see HTTP traffic starting at pakcet `32262`. We can see that it is hosting the web directory `/dvwa`.

![](../../images/discovery_10.png)

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Web Directory
> ```
> /dvwa
> ```

</details>
