# Discovery 11

## Description

What resource was input to the above page?

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* Wireshark

### Walkthrough

This picks up from `Discovery 10`.

I don't really get this one... I think it's worded weirdly... The `resource` that is `input` is `setup.php`, which you can see is eventually accessed.

![](../../images/discovery_11.png)

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Input Resource
> ```
> setup.php
> ```

</details>
