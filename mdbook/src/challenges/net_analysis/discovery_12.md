# Discovery 12

## Description

What port was hosting the 2nd web server on 192.168.0.10?

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* Wireshark

### Walkthrough

This one picks up from `Discovery 4`.

If you scroll down, you'll see a second webpage on port `8080`.

</details>

## Flags

<details>
<summary>Show Flag</summary>

> 2nd web server port
> ```
> 8080
> ```

</details>
