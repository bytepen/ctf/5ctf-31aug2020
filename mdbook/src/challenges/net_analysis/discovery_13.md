# Discovery 13

## Description

Was the status query performed on the 2nd webserver successful?

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* Wireshark

### Walkthrough

This picks up from `Discovery 12`.

If you inspect TCP and scroll down, you'll find a `GET /manager/status` that is met with an `HTTP/1.1 401 Unauthorized`.

![](../../images/discovery_13.png)

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Was the query succcessful
> ```
> No
> ```

</details>
