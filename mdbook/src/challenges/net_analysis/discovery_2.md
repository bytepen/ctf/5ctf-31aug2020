# Discovery 2

## Description

What word was querified for its definition in the web traffic?

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* Wireshark

### Walkthrough

I wasn't entirely sure what I would be looking for with this one, but I figured `querified` meant it was in the URL. I just clicked through all of the TCP streams until I found the one that contained the following line:

> ```
> GET /i/adsct?type=javascript&version=1.1.0&p_id=Twitter&p_user_id=0&txn_id=nvia8&tw_sale_amount=0&tw_order_quantity=0&tw_iframe_status=0&tw_document_href=http%3A%2F%2Fdefinition.org%2Fdefine%2Fdichotomy%2F HTTP/1.1
> ```
> ```
> ...&tw_document_href=http%3A%2F%2Fdefinition.org%2Fdefine%2Fdichotomy%2F...
> ```

This was in packet `6253`.

This reveals to us that someone was searching for the definition of `dichotomy`.

</details>

## Flags

<details>
<summary>Show Flag</summary>

> What word was querified for its definition in the web traffic?
> ```
> dichotomy
> ```

</details>
