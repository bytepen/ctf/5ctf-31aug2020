# Discovery 3

## Description

What OSI Layer 5 functionality is lacking from the first 5 requested websites?

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* Wireshark

### Walkthrough

I don't really know a good way to show this one, but if you scroll through the list of packets while watching the `protocol` pane, you'll start seeing TLS pop up around packet 700. This is a `Session` layer protocol and the flag is `SSL`.

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Lacking Layer 5 Functionality
> ```
> SSL
> ```

</details>
