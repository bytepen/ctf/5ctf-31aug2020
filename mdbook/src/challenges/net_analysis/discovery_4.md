# Discovery 4

## Description

A port scan exists in this pcap. At what time did it begin in CDT?

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* Wireshark

### Walkthrough

My favorite way to find port scans is to filter on `tcp.flags.reset == 1`. Resets occur when a port is closed, which happens a lot in port scans. From there, I filter by port and find common destination IPs. In the picture below, we can determine that `192.168.0.169` is scanning `192.168.0.10` because it's asking for common ports and returning that they're all closed.

![](../../images/discovery_4_reset.png)

Now we can filter on traffic between these two clients with `ip.dst == 192.168.0.10 && ip.src == 192.168.0.169`. We filter by Packet `No.` and see that the first scan was on port `8080` at `11:10:31` CDT.

![](../../images/discovery_4_start.png)

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Time Port Scan Began
> ```
> 11:10:31
> ```

</details>
