# Discovery 5

## Description

What address did the port scan originate from?

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* Wireshark

### Walkthrough

Steps on how to discover the source IP `192.168.0.169` were covered in `Discovery 4`.

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Port Scan Source IP
> ```
> 192.168.0.169
> ```

</details>
