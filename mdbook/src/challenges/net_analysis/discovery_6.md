# Discovery 6

Which protocol did the address connect to and then use?

## Description

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* Wireshark

### Walkthrough

This challenge also picks up where we left of in `Discovery 4`.

If we scroll down, we can see they found creds for `FTP` and connected.

![](../../images/discovery_6.png)

</details>

## Flags

<details>
<summary>Show Flag</summary>

> What Protocol did they use?
> ```
> FTP
> ```

</details>
