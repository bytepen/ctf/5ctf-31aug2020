# Discovery 7

## Description

Which 2 values were input to establish a connection? Answer: value1/value2

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* Wireshark

### Walkthrough

This picks up where we left off in `Discovery 6`. In packets `24597` and `24723`, we can see the username `user` and the passwords `perfection20`.

![](../../images/discovery_6.png)

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Credentials
> ```
> user/perfection20
> ```

</details>
