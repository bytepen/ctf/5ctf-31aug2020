# Discovery 8

## Description

What host resource was accessed?

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* Wireshark

### Walkthrough

This one picks up from `Discovery 7`.

If we walk through the requests made while connected to FTP, we see the following:

```
CWD Users
LIST
CWS assessors
LIST
RETR 14aug20.txt
```

This indicates that they retrieved `14aug20.txt` in packet `25309` as seen below:

![](../../images/discovery_8.png)

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Host Resource
> ```
> 14aug20.txt
> ```

</details>
