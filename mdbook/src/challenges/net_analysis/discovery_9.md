# Discovery 9

## Description

What monetary value was observed?

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* Wireshark

### Walkthrough

This picks up from `Discovery 8`.

We can find the `FTP-DATA` pakcet assoicated with this transfer at `24314`. Inspecting the hex at the bottom tells us `Deposit $1000 in John Dought bonus account.`

![](../../images/discovery_9.png)

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Monetary Value
> ```
> $1000
> ```

</details>
