# All Your Base Are Belong to Root (50 Points)

## Description

This CentOS image has been compromised so any user can become root. We need your help on figuring out what happened.


First question: What file has been exploited to gain root?

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* Terminal

### Walkthrough

We are given a compressed file that can easily be imported into VMWare Workstation. Upon booting and trying some random passwords, we determine that the `user` account's password is `password`.

When we open up terminal, we see a folder names `files`. We can see the contents of this folder below:

![](../../images/all_your_base_are_belong_to_root-001.png)

The files `--checkpoint=1` and `--checkpoint-action=exec=sh net.sh` look pretty interesting. If you're familiar with `tar`, you may recognize that they give you the ability to execute commands periodically while running a tar command.

If we check out the contents of `net.sh`, we can see the following code which creates a pretty basic privilege escallation vector. Essentially, when you set the `suid` bit of something like `/bin/bash`, you are saying that you wish to execute this program as the owner. In this example, you are stating you wish to execute `/bin/bash` as `root` as shown below. This would be done with `/bin/bash -p`.

![](../../images/all_your_base_are_belong_to_root-002.png)

That's interesting and definitely something worth remembering, but it doesn't exactly answer our question for this challenge.

It turns out we have the ability to `sudo su` to root, so we do that and then go check out `root`'s home directory. We find a folder called `adm_scripts` and a file called `backup.sh` inside of that. This script runs a tar command on our home directory's folder. The oddly named files we saw earlier are understood as commands, making it so that `sh net.sh` is run every time that the tar command is run. This can be seen below:

![](../../images/all_your_base_are_belong_to_root-003.png)

There's our answer: `/root/adm_scripts/backup.sh`

To take it one step further, we can determine that this script is run every minute via cron:

![](../../images/all_your_base_are_belong_to_root-004.png)

</details>

## Flags

<details>
<summary>Show Flags</summary>

> What file has been exploited to gain root?
>
> ```
> /root/adm_scripts/backup.sh
> ```

</details>
