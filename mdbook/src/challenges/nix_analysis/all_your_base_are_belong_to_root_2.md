# All Your Base Are Belong to Root 2 (80 Points)

## Description

What command can be ran to gain root?

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* Terminal

### Walkthrough

This challenge is a continuation of the previous one and it is all explained there. As I mentioned, there is a `suid` bit `/tmp/bash` which is owned by `root`. Executing `/tmp/bash -p` drops you into a root shell.

![](../../images/all_your_base_are_belong_to_root_2-001.png)

</details>

## Flags

<details>
<summary>Show Flag</summary>

> What command can be ran to gain root?
>
> ```
> /tmp/bash -p
> ```

</details>
