# nix_analysis

This category only had two questions worth 130 Points.

I enjoyed this category and wish there were more challenges in it.
The only thing that was kind of annoying about it was having to download the 2 GB file, but it really wasn't a big deal.
I'm wondering about the feasability of making it a Docker container running on a remote server with ssh enabled and PKI for a bit of protection.
I'd like to toy around with this idea myself, but to be totally honest, I'm probably going to forget amidst the billions of other things I'm trying to juggle...
Just a thought!
