# Caesars Transmission

## Description

An alphabetical rot() cyper text is being sent across a network, you are tasked with creating a tool to quickly translate text by rotating it programitically. A code is transmitted where each word has a different rotation factor and another code is transmitted to the other user with how much to rotate each word. The second cipher code isn't available so the script must rotate each word manually.

An example: The code, 'ymnx lv c xiwx' is transmitted with the cipher code '5324' Rotating each letter in the first word 'ymnx' backward (the original was rotated foward) 5 characters in the alphabet results in 'this'. Repeating this step with the following words with a backwards rotation of 3, 2, and 4 steps respectively will result in the message: 'this is a test'. The messages are encrypted with a forward rotation factor of 1-5 only. They are only lower case with no punctuation and only contain the 26 characters in the English alphabet (no numbers).

FLAG: Translate the following message into English text.

```
kv jt wemh yct ofwfs dibohft nfo ep vjtqwij vjg spbet vjga ycnm erh vjku vseh ibt vieglih jut foe
```

## Solution

<details>
<summary>Show Solution</summary>

### Tools

</details>

## Flags

<details>
<summary>Show Flag</summary>

</details>
