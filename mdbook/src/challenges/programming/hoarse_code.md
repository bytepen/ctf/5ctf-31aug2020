# Horse Code

## Description

Intel has collected a series of transmissions that seem to resemble a kind of binary morse code. A translation key has been created but there is far too much data to translate by hand. You have been tasked with creating an automated tool to translate the given 'hoarse code' transmission. Download the attached file to receive the transmission the corresopdmig code.

```
Transmission: -..-...-.-.-..---.-.-----.-..---.-.------..-...-.-.-...-----.-.--.-...-.---------..-...---------..-...-.-.-...-------.-.-..-..........--.-.-.-...

Key:
a = .....
b = ....-
c = ...-.
d = ..-..
e = .-...
f = -....
g = ...--
h = ..-.-
i = .-..-
j = -...-
k = .-.--
l = -.-.-
m = --..-
n = -.-..
o = ..---
p = .-.-.
q = -..--
r = .----
s = --.-.
t = -..-.
u = ----.
v = ---..
w = ---.-
x = --.--
y = -.---
z = -.--.
_ = -----

```

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* python

### Walkthrough

I decided the way that I wanted to solve this challenge was to create a dict to lookup each letter, then iterate through the `transmission` 5 letters at a time to use that dict to spell out the message. Here's what I came up with:

```
#!/usr/bin/env python3

transmission = '-..-...-.-.-..---.-.-----.-..---.-.------..-...-.-.-...-----.-.--.-...-.---------..-...---------..-...-.-.-...-------.-.-..-..........--.-.-.-...'

key = {
    '.....': 'a',
    '....-': 'b',
    '...-.': 'c',
    '..-..': 'd',
    '.-...': 'e',
    '-....': 'f',
    '...--': 'g',
    '..-.-': 'h',
    '.-..-': 'i',
    '-...-': 'j',
    '.-.--': 'k',
    '-.-.-': 'l',
    '--..-': 'm',
    '-.-..': 'n',
    '..---': 'o',
    '.-.-.': 'p',
    '-..--': 'q',
    '.----': 'r',
    '--.-.': 's',
    '-..-.': 't',
    '----.': 'u',
    '---..': 'v',
    '---.-': 'w',
    '--.--': 'x',
    '-.---': 'y',
    '-.--.': 'z',
    '-----': '_'
}

answer = ''

for i in range(0, len(transmission), 5):
    answer += key[transmission[i:i+5]]

print(answer)
```

This spit out `this_is_the_key_to_the_stable`.

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Decoded Message
> ```
> this_is_the_key_to_the_stable
> ```

</details>
