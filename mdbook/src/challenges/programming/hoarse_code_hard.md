# Horse Code - Hard

## Description

Intel has found a new trasmission with a different pattern. A copy of the encoder was found but was unable to be decompiled. You are tasked with generating a key for the 'hoarse code' and scripting a tool to translate the transmission below into English text. Note: Experiment with the attached encoder program.

> Transmission
> ```
> ....-|...|..|-..|..-..|-.-|.-..|..-..|.-.|-..-|-...|-...|..|..-..|-.--|..|---|...|..-..|.-..|....-|..-..|-..|-..-|---.|--.-|...|..-..|-.--|-..-|..-..|-.--|-..|...|..-..|-..-|..-.|--|..-..|-.--|-..-|----|-...|..-..|---.|-..-|..|--|..-..|-.-|.-..|..-..|.-.|-..-|-...|-...|..|..-..|---.|-.-|--|...|..-..|-.--|-.-|..-.|..-..|-.-|..-..|-.|..|-...|-.--|..-..|-...|-..-|..-..|.-..|-..-|---.|...
> ```

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* Python

### Walkthrough

In this challenge, we are given an encoder that allows us to input a plaintext message. The program then outputs ciphertext.

The easiest way I could think of to solve this was to get all of the possible letters `a-z` in their encoded form, and then build a quick translator in python. This is what I came up with:

```
a_z = '..|.-|-.|--|...|..-|.-.|-..|-.-|--.|---|..-.|.-..|-...|-..-|-.-.|--..|---.|--.-|-.--|.---|.-...|----|.....|....-|...-.'.split('|')
msg = '....-|...|..|-..|..-..|-.-|.-..|..-..|.-.|-..-|-...|-...|..|..-..|-.--|..|---|...|..-..|.-..|....-|..-..|-..|-..-|---.|--.-|...|..-..|-.--|-..-|..-..|-.--|-..|...|..-..|-..-|..-.|--|..-..|-.--|-..-|----|-...|..-..|---.|-..-|..|--|..-..|-.-|.-..|..-..|.-.|-..-|-...|-...|..|..-..|---.|-.-|--|...|..-..|-.--|-.-|..-.|..-..|-.-|..-..|-.|..|-...|-.--|..-..|-...|-..-|..-..|.-..|-..-|---.|...'

decoded = ''

for letter in msg.split('|'):
    if letter == '..-..':
        letter = '_'
    else:
        letter = chr(a_z.index(letter) + 97)
    decoded += letter

print(decoded)
```

This spit out `yeah_im_gonna_take_my_horse_to_the_old_town_road_im_gonna_ride_til_i_cant_no_more`

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Decoded Message
> ```
> yeah_im_gonna_take_my_horse_to_the_old_town_road_im_gonna_ride_til_i_cant_no_more
> ```
</details>
