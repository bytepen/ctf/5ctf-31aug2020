# Beginning RE

## Description

Download the attached executable. What language is it written in?

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* strings

### Walkthrough

The easiest way I know of to solve this challenge is just to run `strings` on the file.
Running `strings ./BeginRE.exe` returns the following:

```
/lib64/ld-linux-x86-64.so.2
libstdc++.so.6
__gmon_start__
_ITM_deregisterTMCloneTable
_ITM_registerTMCloneTable
_ZNSirsERi
_ZNSolsEj
_ZNSaIcED1Ev
_ZSt24__throw_invalid_argumentPKc
_ZSt3cin
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1EPKcRKS3_
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEpLEPKc
_ZNSt8ios_base4InitD1Ev
__gxx_personality_v0
_ZNSaIcEC1Ev
_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE5c_strEv
_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
_ZNSt8ios_base4InitC1Ev
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEpLERKS4_
_ZSt4cout
_ZSt20__throw_out_of_rangePKc
libgcc_s.so.1
_Unwind_Resume
libc.so.6
getpwuid
strtol
__cxa_atexit
__errno_location
geteuid
__cxa_finalize
__libc_start_main
__xstat
GCC_3.0
CXXABI_1.3
GLIBCXX_3.4
GLIBCXX_3.4.21
GLIBC_2.2.5
u/UH
gfffH
[]A\A]A^A_
stoi
Stay a while and listen!
The Cake is a Lie
I've heard enough
I'm a computer!
Don't point that thing at me!
/home/
/FiveCTF/BeginRE.exe
...
```

We can see a handful of results such as `libc.so.6` and `GCC_3.0` that give away that this program was written in the `C` programming language.

</details>

## Flags

<details>
<summary>Show Flag</summary>

> What Programming Language was this written in?
> ```
> C
> ```

</details>
