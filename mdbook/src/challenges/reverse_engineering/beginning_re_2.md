# Beginning RE 2

## Description

How many user defined functions are there? Include the program entry function in your answer

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* Ghidra

### Walkthrough

I never actually got a solid answer for this one, and it took a bit of guess work.
I'll show how I got there anyway.

I started off with running `file ./BeginRE.exe`, which reported the following:

```
BeginRE.exe: ELF 64-bit LSB shared object, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=5178e3442f7d86d0d2a4503beafb93928a18caf9, for GNU/Linux 3.2.0, not stripped
```

The most important part of this is that it is `not stripped`, that means that it still includes the debugging symbols and inspection in Ghidra should be a breeze.
Another very interesting thing to note is that it is suffixed with `.exe`, but it is an `ELF 64-bit LSB shared object`.

I first created a new project for `5ctf` in Ghidra by going to `File` => `New Project`.
I'll use this project for all of the RE challenges in this CTF so I can easily switch between them.
I then imported `BeginRE.exe` by going to `File` => `Import File` and selecting it.
Because it has the debugging symbols, I will opt to leave the default settings.

![](../../beginning_re_2-001.png)

An `Import Results Summary` will appear as follows:

![](../../beginning_re_2-002.png)

Note that it says there are `73` functions.
This is the incorrect answer because the question asks for `user defined functions`.

When prompted, choose the analyze the file.
You may find it helpful to also tick `Decompiler Parameter ID` under `Analyzers`, but this one shouldn't need it.
I like to enable it anyway.

When it's done, you should see a screen similar to the following:

![](../../beginning_re_2-003.png)

You can expand `Functions` on the left to see all the functions that were found.
When counting the user defined functions, I looked for functions that had names that I (or Google) didn't recognize as standard C functions.
Additionally, I found that they were pink.
Finally, when clicking on them and inspecting the decompiled code, certain functions just felt more "human-written" as seen below:

![](../../beginning_re_2-004.png)

I found the following functions were `user-defined`:

```
main
findDig
gU
init
x86
retFlag
enuf
speak
```

If you count the list of functions, we have `8`.

There is one function here that stands out among the rest as the "most important" to find.
This is the `main()` function.
This function is the first function to be called during the execution of this program and is a good place to start looking when you first look into a program.
Note that in some circumstances, it may be called `entry()` in ghidra.

One other way that this *should* be able to be solved is by going to the `main()` function and couting the functions that are referenced as seen below:

![](../../beginning_re_2-005.png)

I only see 7 here, so you may have mixed success with this method.

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Number of User Defined Functions
> ```
> 8
> ```

</details>
