# CrackME 03

## Description

CrackME program. Can you input the correct password?

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* ghidra

### Walkthrough

I definitely didn't solve this one as intended, but I got it!

I have found that in certain circumstances, you can patch programs so that they'll execute after you've modified their assembly. In this case, I skipped right to the functionality that told me the flag before it even checked my password.

In order to do this, import the file into ghidra, however, you MUST change the format from `ELF` to `RAW` and set the `Language` to what it was when it was `ELF`.

![](../../images/crackme_3_001.png)
![](../../images/crackme_3_002.png)

For reference, you may also wish to import the file normally as `ELF` as well. This will NOT be the version we are patching, but it helps figure things out sometimes since they look different in ghidra.

Once this is done, open them and make sure that you analyze them with standards + `Decompiler Parameter ID` (not necessary, but helps sometimes).

![](../../images/crackme_3_003.png)

In the `ELF` copy, we can find that the `main` function starts around `00101229`.

![](../../images/crackme_3_004.png)

We can take this information and in the `RAW` copy, go to `00001229` to find `main`.

![](../../images/crackme_3_005.png)

Comparing the `ELF` and the `RAW` code, we can see that the `RAW` decompilation defines `printf` as `FUN_000010f0()`. We can also see that this is accessed with a `CALL` in assembly. This draws our attention because it is called for the win condition, so if we could jump straight there, perhaps we could just win right off the bat.

![](../../images/crackme_3_006.png)

Now we can find a good place to call it. If we look at the `CALL`'s earlier in the code, we can see some at `strcat()`, `fgets()`, `popen()`, and `pclose()` functions. Looking into these would show us that the variables toward the top of the function are `base64` and the `popen` is calling `echo '...' | base64 -d`. Best not to mess with those. Shortly after, however, we find `strlen()` at `00101b0d`, which is just being used as part of the password check. Bye!

![](../../images/crackme_3_007.png)

Back in the `RAW` function, we navigate to `00001b0d` and press `Ctrl+Shift+G` to `Patch Instruction` (this can also be done in the right click menu.) and change `0x000010d0` to `0x000010f0`. We save our binary by pressing the `o` key and ensure we select `Format` to be `Binary`.

![](../../images/crackme_3_008.png)
![](../../images/crackme_3_009.png)

We can then navigate to the folder that we saved it in and execute it with whatever password we want. Out pops the flag `flag{I_gu3ss_that_was_hard3r...}`.

![](../../images/crackme_3_010.png)

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Correct Password Flag
> ```
> flag{I_gu3ss_that_was_hard3r...}
> ```

</details>
