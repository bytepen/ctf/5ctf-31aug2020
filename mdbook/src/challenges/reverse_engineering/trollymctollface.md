# TrollyMcTrollFace

## Description

What unique flag grants you access?

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* ghidra
* edb
* strace
* strings

### Walkthrough

This one took me quite a while and I didn't actually finish it during the ctf.
It isn't incredibly complex once you know where to look, but there is quite a bit going on and many rabbitholes to get lost down on your way to the flag.
There are 9 flags, and I will go over how to find each of them.
This will be sorted primarily by the method used to find them.

#### Strings

Strings is a great place to start with any binary you are trying to understand.
They can be viewed within `ghidra` if you're running solely on Windows, but also via the linux `strings` command.
Running `strings ./TrollyMcTrollFace` will return the following:

```
/lib64/ld-linux-x86-64.so.2
libcurl.so.4
...
curl_easy_cleanup
curl_easy_init
curl_easy_setopt
curl_easy_perform
libc.so.6
...
check 2
Congratulations!!! The flag is %s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s
Access Granted!  Please submit your flag
Enter the password:
You entered: %s
Access denied!!!
Error: That's not supposed to happen. Tell WhiteCell
/tmp/flag.txt
:*3$"
flag{all_ur_base64_r_belong_to_us}
'gm`fzxnt^b`o^fn^vhui^uihr^YNS^xnt^b`o^fn^vhui^ui`u|'
https://grimelords.us/flag.txt
Zmxh
Z3tz
cGVh
a19w
bGFp
bmx5
X3Bs
ZWFz
ZX0K
...
```

We can see a handful of flags here, but we can also get insight into some other functionality of the program.
As an example, we can see a lot about `curl` and a URL within the strings.
This tells us, that the specified file is probably going to be read/downloaded.

##### flag{all_ur_base64_r_belong_to_us}

This flag was found in the strings above in plaintext

##### flag{you_can_go_with_this_XOR_you_can_go_with_that}

In the strings above, you can see the following line:

```
'gm`fzxnt^b`o^fn^vhui^uihr^YNS^xnt^b`o^fn^vhui^ui`u|'
```

I found this one by using the CyberChef `Magic` Operation as can be seen [here](https://gchq.github.io/CyberChef/#recipe=Magic(3,true,false,'flag%7B')&input=Z21gZnp4bnReYmBvXmZuXnZodWledWlocl5ZTlNeeG50XmJgb15mbl52aHVpXnVpYHV8).
This revealed that it was an XOR operation.
Additionally, if I knew that it would/could be, I could use the `XOR Brute Force` function seen [here](https://gchq.github.io/CyberChef/#recipe=XOR_Brute_Force(1,100,0,'Standard',false,true,false,'flag%7B')&input=Z21gZnp4bnReYmBvXmZuXnZodWledWlocl5ZTlNeeG50XmJgb15mbl52aHVpXnVpYHV8).

If I have no idea what's going on, I like to start with `Magic` because, well, it's magic!
It can really help you solve a problem pretty quick.
Note that these both have `flag{` set in the `Crib` field.
This helps narrow down your results a LOT.

As a side note, `^` usually denotes an `XOR` operation and there are a lot of them in this flag.
I don't know if this was a hint or a coincidence, but given that any XOR key could have been used, I believe this is a hint.

##### flag{speak_plainly_please}

Usually when running strings, I like to throw in the `-n10` option, which gets rid of any string shorter than 10 characters.
This time, however, you would have missed a flag as the following lines are only 4 characters:

```
Zmxh
Z3tz
cGVh
a19w
bGFp
bmx5
X3Bs
ZWFz
ZX0K
```

This one is also pretty easy to miss because it isn't super obvious.
The reason I caught it is that it is rare to see a collection of consecutive giberish lines with the same length and no special characters in strings.

If we put them all together, we get `ZmxhZ3tzcGVha19wbGFpbmx5X3BsZWFzZX0K`.
You MAY be able to recognize this as base64 right off the bat.
We can use something like the CyberChef recipe [here](https://gchq.github.io/CyberChef/#recipe=From_Base64('A-Za-z0-9%2B/%3D',true)&input=Wm14aFozdHpjR1ZoYTE5d2JHRnBibXg1WDNCc1pXRnpaWDBL) or via the linux `base64` command like `printf '...' | base64 -d`.

##### flag{never_forget_your_keys_at_home}

You may notice a URL and a file within strings as well:

```
/tmp/flag.txt
...
https://grimelords.us/flag.txt
```

Visiting this URL will give you this flag, but there is also a way to find it in `/tmp/flag.txt`.
I will delve into this more in a minute, but for now, I'll just state that certain attempted passwords will result in this file being created with the contents of the website.

#### Ghidra

This didn't actually end up being directly used to find the final flag, but it was incredibly helpful in determining how the program works and using Ghidra was critical to my understanding of the program so that I could eventually reach the final flag.

Note that I found it very useful to have Trolly imported with both `RAW` and `ELF` Formats.
Some pieces to the puzzle may be easily seen in `ELF` and others seen only in `RAW`.
Additionally, if you get into `patching` the program as I mention later on, you may only have success when patching the `raw` format.
Make sure that you note the `Language` when switching to `raw` as it will get blanked.

When importing, make sure you Analyze the binary.
While not strictly necessary, sometimes I've had better luck while leaving all the defaults plus turning on `Decompiler Parameter ID`.

> Elf
> ![](../../images/trollymctrollface-001.png)

> Raw
> ![](../../images/trollymctrollface-002.png)

Note that in the `raw` format, none of the functions have names at all, but in the `elf` format, we can find the `entry()` function.
Note the address of `000012a0` in `raw` and `001012a0` in `elf`.
I don't know exactly why, but the addresses are `00100000` higher in the `elf` format.
Other than that, they line up.

At this point, if you followed along with the other RE write-ups here, you may notice there is no `main()` function.
The `entry()` function above is the first function that is called.

Note the following line in the pseudocode:

```
__libc_start_main(&LAB_00101f2f,in_stack_00000000,&stack0x00000008,&LAB_00102360,&DAT_001023d0,param_3,auStack8);
```

[Researching](https://refspecs.linuxbase.org/LSB_3.1.0/LSB-generic/LSB-generic/baselib---libc-start-main-.html) this function tells us that the first parameter is `main()`, so navigate to `00101f2f`.

> Elf
> ![](../../images/trollymctrollface-003.png)

> Raw
> ![](../../images/trollymctrollface-004.png)


Note that this is another perfect reason why you should import as both `raw` and `elf`, as you will notice there is no pseudocode in the `elf`, but it is there in the `raw` format.

This is also the perfect time to demonstrate some ghidra functionality that you MUST become familiar with.
Click on the `00101f2f` line and press the `l` (lower `L`) key.
This will prompt you to rename the function so that when you see it later, you don't have to think about "what exactly 00101f2f means again?", you just instantly see it is `main()` and can understand exactly what is going on.
I highly recommend spending some time at the very beginning of your analysis to dig in and try and figure out as many `FUN_00000000`'s and `LAB_00000000`'s as possible while renaming them in the process.
Note that you can also rename variables, such as `cStack1368` and `iVar2`, but sometimes they get re-used, so make sure you don't end up confusing yourself.
For this reason, I try to avoid renaming variables at first until I get everything else labeled and I can see exactly how they're used.
Renaming everyhing will not only help familiarize you with the program, but will make stuff like the following picture much more understandable:

![](../../images/trollymctrollface-005.png)
![](../../images/trollymctrollface-006.png)

When going through some of the code, it's very possible that you will find something that you have no ability to understand.
I still like to give this some sort of a name just so I remember what that is.
I named the following function `hard_to_understand_function`.

![](../../images/trollymctrollface-007.png)

Note that it's probably a very good idea to save after you go through all this renaming.

Now that we have cleaned this up a little bit, let's try and understand what this `main()` function is doing.
This is the decompiled code that we have and it's still pretty hard to understand at points, but we'll do our best.

```
{
  code cVar1;
  int iVar2;
  undefined8 uVar3;
  ulong uVar4;
  long lVar5;
  byte bVar6;
  undefined8 extraout_RDX;
  long lVar7;
  long in_FS_OFFSET;
  int iStack1432;
  int iStack1428;
  char cStack1368;
  char cStack1367;
  char cStack1366;
  char cStack1365;
  char cStack1364;
  long lStack16;
  
  lStack16 = *(long *)(in_FS_OFFSET + 0x28);
  plt.sec();
  __isoc99_scanf();
  plt.sec();
  if ((((cStack1368 == 'F') && (cStack1367 == 'l')) && (cStack1366 == 'a')) &&
     ((cStack1365 == 'g' && (cStack1364 == '{')))) {
    uVar3 = twists_and_turns();
    if ((int)uVar3 == 0) {
      uVar3 = 0;
    }
    else {
      puts();
      uVar3 = 1;
    }
  }
  else {
    if ((((cStack1368 == 'f') && ((cStack1367 == 'l' && (cStack1366 == 'a')))) &&
        (cStack1365 == 'g')) && (cStack1364 == '{')) {
      curl_grimelords();
      uVar3 = get_memory_address();
      if ((int)uVar3 == -1) {
        fwrite();
        uVar3 = 1;
      }
      else {
        uVar4 = fopen();
        if (uVar4 == 0) {
          uVar3 = 1;
        }
        else {
          fseek();
          ftell();
          fseek();
          lVar5 = calloc();
          if (lVar5 == 0) {
            uVar3 = 1;
          }
          else {
            lVar7 = 1;
            fread();
            fclose();
            iVar2 = strlen();
            iStack1432 = 0xc;
            iStack1428 = 0;
            while (bVar6 = (byte)uVar4, iStack1432 < 0x2a0) {
              cVar1 = hard_to_understand_function[iStack1432];
              uVar4 = (ulong)(byte)cVar1;
              *(byte *)(hard_to_understand_function + iStack1432) =
                   *(byte *)(lVar5 + iStack1428 % iVar2) ^ (byte)cVar1;
              iStack1432 = iStack1432 + 1;
              iStack1428 = iStack1428 % iVar2 + 1;
            }
            putchar();
            iVar2 = hard_to_understand_function(&cStack1368,lVar7,extraout_RDX,bVar6);
            if (iVar2 == 0) {
              uVar3 = 0;
            }
            else {
              puts();
              uVar3 = 1;
            }
          }
        }
      }
    }
    else {
      puts();
      uVar3 = 1;
    }
  }
  if (lStack16 != *(long *)(in_FS_OFFSET + 0x28)) {
    uVar3 = __stack_chk_fail();
  }
  return uVar3;
}
```

One thing that should be pretty easy to spot and understand is the primary `if` clauses.
By examining the first one and the sequential variables, we can come to the assumption that this is checking that the password starts with `Flag{`.
There are a couple ways that we can confirm this, and I'll get into one of them in the `EDB` section, but just take my word for it for right now (the other is that we could patch the `CALL` to `twists_and_turns` to call something like we do for the `never_forget_your_keys` flag below and check the patched call is only ever returned when we enter `Flag{`.

We can also make the quick determination that this code only cares about the first 5 characters.

We see no "win conditions" in this statement since the `twists_and_turns` flag isn't accepted as our answer, so we can determine that it's probably not super intereting to us and that our password doesn't start with `Flag{`.

The `else` clause leads us to another `if` clause which checks for `flag{` in the same way.
If it doesn't, it `puts()` our `Access denied!!!` string and fails (`uVar3 = 1`, where `uVar3` is the exit code of the `main()` function).

With that, we can make the determination that our answer MUST start with `flag{`.
I don't fully understand what is going on with `get_memory_address`, but if we check that `/tmp/flag.txt` is being updated as explained below in the `never_forget_your_keys` flag below when `flag{` is entered, then we can determine that it always executes the `else` clause and we don't REALLY need to worry about this.
I THINK what this is doing is checking that the flag was successfully downloaded from the website and telling you to contact `WhiteCell` because an `error` happened.

This brings us to our next `if` clauses, which confirm that the file `/tmp/flag.txt` can be opened and read.
From there, we are brought to our innermost `else` clause.
This is where the real magic happens.
It is also where I personally started to lose the ability for ghidra to help me much.

This is a good time to throw in the `strace` command to show how it can help us make a determination in this case.
If we run the command `strace TrollyMcTrollFace`, and enter `Flag{`, we can see it is closed immediate, however, if `flag{` it entered, more stuff is run, to include downloading our flag to `/tmp/flag.txt`:

![](../../images/trollymctrollface-023.png)
![](../../images/trollymctrollface-024.png)

We can see that `strlen` is called, which we can assume is the length of our flag stored in `/tmp/flag.txt` from the `fread` line right above it. We can tell that `iStack1432` appears to be some sort of counter that will exit the while loop after `0x2a0 - 0x12` iterations. We can see both `iStack1432` and `iStack1428` incrementing by 1 (plus a bit of `%`'ing) which tells us these both operate as some kind of counter, and we can see an XOR (`^`) operation taking place.
I'm sure it's possible for someone who knows exactly what they're doing to reverse engineer this statically, but that's a little beyond me.
Instead, I'll pick up from here with `EDB` after I explain the Ghidra flags.

I feel like that's enough housekeeping for right now, let's dig into some flags!

##### flag{twists_and_turns}

You may have noticed a very long function that starts like this in the renaming step:

```
{
  char cVar1;
  undefined8 uVar2;
  ulong uVar3;
  char *pcVar4;
  long in_FS_OFFSET;
  undefined2 uStack57;
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  uVar3 = 0xffffffffffffffff;
  pcVar4 = (char *)((long)&uStack57 + 1);
  do {
    if (uVar3 == 0) break;
    uVar3 = uVar3 - 1;
    cVar1 = *pcVar4;
    pcVar4 = pcVar4 + 1;
  } while (cVar1 != '\0');
  *(undefined2 *)((long)&uStack57 + ~uVar3) = 0x46;
  uVar3 = 0xffffffffffffffff;
  pcVar4 = (char *)((long)&uStack57 + 1);
  do {
    if (uVar3 == 0) break;
    uVar3 = uVar3 - 1;
    cVar1 = *pcVar4;
    pcVar4 = pcVar4 + 1;
  } while (cVar1 != '\0');
  *(undefined2 *)((long)&uStack57 + ~uVar3) = 0x6c;
  uVar3 = 0xffffffffffffffff;
  pcVar4 = (char *)((long)&uStack57 + 1);
  do {
    if (uVar3 == 0) break;
    uVar3 = uVar3 - 1;
    cVar1 = *pcVar4;
    pcVar4 = pcVar4 + 1;
  } while (cVar1 != '\0');
  *(undefined2 *)((long)&uStack57 + ~uVar3) = 0x61;
  uVar3 = 0xffffffffffffffff;
  pcVar4 = (char *)((long)&uStack57 + 1);
  do {
    if (uVar3 == 0) break;
    uVar3 = uVar3 - 1;
    cVar1 = *pcVar4;
    pcVar4 = pcVar4 + 1;
  } while (cVar1 != '\0');
  *(undefined2 *)((long)&uStack57 + ~uVar3) = 0x67;
  uVar3 = 0xffffffffffffffff;
  pcVar4 = (char *)((long)&uStack57 + 1);
  do {
    if (uVar3 == 0) break;
    uVar3 = uVar3 - 1;
    cVar1 = *pcVar4;
    pcVar4 = pcVar4 + 1;
  } while (cVar1 != '\0');
  *(undefined2 *)((long)&uStack57 + ~uVar3) = 0x7b;
```

There's quite a lot to take in here, but it's easy to understand if you don't worry about fully understanding it.
There are 8 hex characters that you are going to become super familiar with in this challenge.
They are `466c61677b` and if you so something like `bytes.fromhex('466c61677b')` in python, you will see `b'Flag{'` returned.

Alternatively, if you hover over one of these, such as `0x46`, ghidra will show you the ascii for it.
Note that sometimes it will also tell you what it is in the right side of the assembly pane.
This is not one of those times.

![](../../images/trollymctrollface-008.png)

If we go through each of the characters here, we get the hex string `466c61677b7477697374735f616e645f7475726e737d`, which is `Flag{twists_and_turns}`.
Note that at one point, we are given `... = 100` instead of `... = 0xXX`.
Since everything else is hex, I made sure to convert this to the hex (`0x64`) before adding it to my hex string.

##### flag{never_forget_your_keys_at_home} (again)

This flag was brought up earlier, but you should have come across it again in your examination of the functions.
It looks like this:

```
{
  FILE *__stream;
  long lVar1;
  undefined8 *puVar2;
  long in_FS_OFFSET;
  undefined8 local_1018;
  undefined8 local_1010;
  undefined8 local_1008 [511];
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  local_1018 = 0x616c662f706d742f;
  local_1010 = 0x7478742e67;
  lVar1 = 0x1fe;
  puVar2 = local_1008;
  while (lVar1 != 0) {
    lVar1 = lVar1 + -1;
    *puVar2 = 0;
    puVar2 = puVar2 + 1;
  }
  lVar1 = curl_easy_init();
  if (lVar1 != 0) {
    __stream = fopen((char *)&local_1018,"wb");
    curl_easy_setopt(lVar1,0x2712,param_1,0x2712);
    curl_easy_setopt(lVar1,0x4e2b,&LAB_00101a7a,0x4e2b);
    curl_easy_setopt(lVar1,0x2711,__stream,0x2711);
    curl_easy_perform(lVar1);
    curl_easy_cleanup(lVar1);
    fclose(__stream);
  }
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return 0;
}
```

There are some hex strings we can convert to text, such as `616c662f706d742f` (`alf/pmt/`) and `7478742e67` (`txt.g`).
After some though, you should recognize this as the `/tmp/flag.txt` string we saw earlier.
Without getting TOO into the weeds, note that it is backwards.
You can think about how computers read by considering a stack of papers.
First it will read `/` and put the paper down, followed by `t`, which will be placed on top of `/` and `m`, which is placed on top of `t`.
So on and so on.
As far as I can tell, they're two variables in the source code to make it harder to decipher.

We can tell that `__stream` is this `/tmp/flag` file opened in `write binary` mode, then some `curl` operations are performed.
Some [Googling](https://www.google.com/search?q=curl_easy_setopt+0x2712+0x432b+0x2711) would lead us [here](https://autohotkey.com/board/topic/20855-libcurl-example/), which tells us that `0x2712` is `CURLOPT_URL` and sets the url, `0x4e2b` is `CURLOPT_WRITEFUNCTION` and handles writing of files, and `0x2711` is `CURLOPT_WRITEDATA` and handles the actualy data being written to our file `__stream`.

With all of that said, if this function is run and you cat `/tmp/flag.txt`, you will get this flag.

##### flag{sometimes_it_really_IS_that_easy}

This flag is not actually used by the code, so it won't immediately pop out at you if all you're doing is looking at `main()`.
To find this one, I was looking at the `Defined Strings` in Ghidra and saw `Congratulations!!! The flag is %s...`. I double clicked this string to take me to its definition in ghidra and saw `XREF[1]: 00101a5f(*)` to the right.
This tells us it is used there and if we double click that, we are brought there.
If you are paying attention, you may noticed a bunch of comments to the right above this line.

![](../../images/trollymctrollface-009.png)

This looks a whole lot like `flag{` upside down.

There are a lot of repeated characters here, but with a little effort, you can extract this flag.

While it's not necessary at all for this flag, I want to get into patching a little bit if for no other reason than to have it in my notes.
Patching in Ghidra refers to the ability to change the assembly to do what you want it to do instead of what it wants to do by default.

As a quick reminder, you will want to do this in the `raw` format.

This is an art more than a science, and I don't fully understand it, however, what I do understand is that if you go to the top of this function (denoted by `LAB_00101920` and the `ENDBR4` assembly instruction), you can gather that this is run when `00001920` is run.
If we look back at `main()`, with enough inspection of the assembly, we can noticed that when the `CALL` assembly is used, it often leads to something that starts with our `ENDBR4`.

![](../../images/trollymctrollface-010.png)
![](../../images/trollymctrollface-011.png)

I'm not overly worried about breaking the program and just want it to spit out this flag, so I'm just going to take the first `CALL` in `main()` and patch it.
I chose the `CALL` as `00001f59`.
We can press `Ctrl+Shift+G` and change `0x00001170` to `0x00001920` and press enter.
We can then press `o` and export it.
Make sure you select `binary` and save it to a spot you'll be able to execute.

![](../../images/trollymctrollface-012.png)

From there, you can execute the program and this flag that would otherwise never be called is output:

![](../../images/trollymctrollface-013.png)

Unfortunately, the rest of our program is now broken, so this `elf` is basically useless to us.
Note that this will happen quite often with patching, and in a lot of cases, it will result in a `segmentation fault` or other results that basically break the entire program.

##### flag{don't_push_me_around}

The flag can be found in much the same way as the previous one.
I saw the string 'Access denied!!!' was used in three different places; `00001fe8`, `000022e8`, and `000022fb`.
`00001fe8` is called during normal circumstances in `main()` (when `Flag{...` is entered as a password. We will get into this later.)
`000022fb` is used at each character check of the password `flag{...` (also holding off on this until later).
Finally, `000022e8` is used if the `hard_to_understand` function I defined earlier is does not return `0` (0 == success).

We see it by looking above the string we're interested in and we will see something that should look familiar.
This time it's read top to bottom and starts with `666c61677b` (`flag{`).
The whole string is `666c61677b646f6e27745f707573685f6d655f61726f756e647d`, which the python `bytes.fromhex('...')` tells us is `flag{don't_push_me_around}`.

The joke behind the flag is that it's done using a bunch of `MOV` instructions.

![](../../images/trollymctrollface-014.png)

#### EDB

After getting as far as I can with ghidra and getting a basic understanding of the way the program works, I'll pick up with EDB.
EDB is a linux GUI program debugger.
The easiest way to use it is to either be on Linux or run Linux in a VM on Windows.
I'm sure this type of analysis could also be done in `GDB`, but I tried and failed to understand how to use it.
`GDB` is definitely a tool I plan on/need to get better with as it is generally more available than `EDB`.

I recommend opening the application in `EDB` and just kind of familiarizing yourself with what it does and how it works first.

![](../../images/trollymctrollface-015.png)

When you first open the program, you will be brought to the screen seen above, with the assembly on the left and a terminal on the right. In the top left corner, you will see `Step Into` (`F7`) and `Step Over` (`F8`)  buttons that I used a lot.
Hold down `F8` (`Step Over`) until the `Enter the password:` prompt appears.
As we determined with ghidra, we want to enter `flag{`, but first, let's enter `Flag{` so we can see the shorter path and see the `twists_and_turns` function.

Type `Flag{` in the right terminal and press enter.
In the left terminal, press `F8` repeatedly while comparing the assembly with `ghidra` and trying to understand what is going on at each step.
You should see it compare each letter of your password to `Flag{` with the `JNE` (`Jump Not Equal`) instructions and then call `twists_and_turns` with `CALL 0x55f60ed4a4d3`.
I don't know if this address is the same every time, but the line I'm talking about can be seen here:

![](../../images/trollymctrollface-016.png)

Press `F7` (`Step Into`) to walk through this function instead of skipping over it.

![](../../images/trollymctrollface-017.png)

You should be able to compare the assembly in `EDB` to the assembly in ghidra and see they are the same.
At this point, the `twists_and_turns` flag can almost certainly be pulled from memory with `gdb -p <PID_of_Trolly>`, but I couldn't figure it out.

Press `F8` through the rest of this while trying to understand the assembly and you'll see `Access denied!!!` get printed and the application exit.

Now we will try with our `flag{` password.
Open Trolly again and hold `F8` until you enter `flag{`.
Hold `F8` until you are brought to the while loop.
You can tell then this is happening because the same assembly will loop over and over and over.
Note that I had an issue where if I JUST held down `F8` the whole time, it wouldn't ever enter the while loop.
I suspect this may be because of some race condition.
If it happens to you, just try again and try holding down `F8` for shorter periods of time.

If you get an error about dividing by `0` at `idiv dword`, the flag is failing to download from `grimelord.us`.
You can get around this by progressing until you get to the five `JNE` instructions that are checking for `flag{`, pressing `F8` until you get JUST passed the next `CALL` (`CALL 0x55ef11636d5b`) and then running `echo "flag{never_forget_your_keys_at_home}" > /tmp/flag.txt` in a new terminal window.
This may occur if the flag is no longer hosted at the URL, but it is currently still hosted and I'm currently having this issue, so your mileage may vary.

The XOR loop can be seen below and ends at the `JL` instruction, which will put you back to the `MOV` instruction at the top:

![](../../images/trollymctrollface-018.png)

If you click on the `MOV` instruction JUST after the `JL` instruction and press `F4` (`Run to this line`), you will be brought to exit of the file loop.
If we compare our `EDB` to `ghidra`, we will see that immediately after this, the `putchar()` function is run (the observent among you will notice that a new line is written to the console), then the `hard_to_understand_function()` function is run.
This is one of the last pieces of the puzzle that we do not understand, and `EDB` will shine some light on it for us.

![](../../images/trollymctrollface-019.png)

Press `F8` until the line with the second call is highlighted, then press `F7` to step into it.

![](../../images/trollymctrollface-020.png)
![](../../images/trollymctrollface-021.png)

If we look at the assembly in the code above, I would hope that by now the hex characters stick out to you.

Now that we have gotten this far, we can find out last 2 flags.

##### flag{following_discretion_is_important}

Picking up where we left off, we can extract the hex characters `666f6c6c6f77696e675f64697363726574696f6e5f69735f696d706f7274616e747d`, which is `following_discretion_is_important}` and is the end of our 8th flag.

Surely this must be it, right!?! Wrong!

Who says "following discrection is important"? Nobody!

Furthermore, if we run Trolly one more time, paste `flag{following_discretion_is_important}` in as our password, and follow along with the code inside this function, we will see something very interesting happen.
It will break the the `JNE` instruction after it evaulates `following_di`.
`s` is not the character we are looking for, so what gives!?!

##### flag{following_directions_is_important}

Does "following directions is important" sound better? It should and you should.

If you inspect the other hex value within this function, you will notice that at first it is incrementing by 1 just before each `CMP` instruction is performed.
This sounds a whole lot like a counter...
Or maybe it's the order of the index of the string it's comparing!

If we examine the assembly, we can see that it starts jumping around when it gets to the `s` comparison.
We need to follow the same directions the machine is following in order to get the flag in the correct order.

The easiest way to solve this is just to realize that `discretion` uses all the same characters as `discretion` and trying that, but failing that, we can just write out the hex we get as we count up the `add` instruction numbers.
This gives us `666f6c6c6f77696e675f646972656374696f6e735f69735f696d706f7274616e747d`, which is `following_directions_is_important}` and is FINALLY our desired flag!

### Why you so hard, `hard_to_understand_function()`!?!?!

So what's going on here?
The trick is in the XOR while loop.
If you notice it is repeating until the first counter reaches `0x2a0`, the `0x2a0` comes from the distance between the function I labled as `hard_to_understand_function` starting on `00001ab8` and the next function ghidra had a chance of understanding, `curl_grimelords()` starting on `00001d5b`.

`0x2a0 == (undefined *)curl_grimelords - (undefined *)hard_to_understand_function - 3`

Note that `(undefined *)` returns the address of those two functions.
There's not really an easy way to understand where that number came from without looking at the source code, but what you CAN notice is that when you go to `hard_to_understand_function` on `00001ab8`, you will see a LOT of line that ghidra can't do much with. 
You may also notice that in `main()`, the XOR line is using a lot of address pointers:

```
*(byte *)(hard_to_understand_function + iStack1432) = *(byte *)(lVar5 + iStack1428 % iVar2) ^ (byte)cVar1;
```

You may also note that it is being increased by `iStack1432`, which is incremented by 1 each time.
We can also note that `iStack1432` started as `0xc`, and there are 12 bytes of instructions between the memory address of `hard_to_understand_function` and the random bytes ghidra can't understand.
All of this points us to the fact that all these bytes that ghidra can't understand are XOR'd in order to do something.

I tried creating the following python code to try and undo this XOR'ing manually, but I've spent way too much time on it and it's still got a couple problems somewhere.
In theory, something like this could extract the actual assembly that would be run statically and then you could dump it into something like [CyberChef's Disassembler](https://gchq.github.io/CyberChef/#recipe=Disassemble_x86('64','Full%20x86%20architecture',16,0,true,true)&input=ZjMwZjFlZmE1NTQ4ODllNTQ4ODNlYzEwNDg4OTdkZjg0ODhiNDVmODQ4ODNjMDA1MGZiNjAwM2M2NjBmODU3OTAyMDAwMDQ4OGI0NWY4NDg4M2MwMDYwZmI2MDAzYzZmNjM4ZjZiMDQxYzE1NDM5ODU2ZWY2NWJhYzkxYTFhYjQxMTE3NGExOTlmNTQyZjM0MGU1NDgxNjljNjVkYThmNzBmMGRiZTE4MjcwYTA4OGU1YTBiMWUxODQ4OGY3ZmVjNzg5N2M4MWUxYzhjMGQwYzYzMTJhZjM0MzgxMjE2NmU5OTZlYzY1NGIzYzUwMDFmYjUxMTU3NzYxODhhMTgwZjBiMTc2MTg4NThkNTQ5ODljNjMzMTNhZDJhMzc1OTExOTIyMTFhM2ExODRmOGI0Y2UzN2E4ZWQyMDcwNmFhNmQyMTZjMGI5NGVhMWQzYTEwNDI4YjdkZmI1M2FlZGUwNzBlOWIyNjM4NzcwNGE5ZDUwNTBkMmM3Zjg1NWNjMjVkOGFjMTBhMTVjNzA4MzU0ODBkOGNmZjAyMTkxNzVkYjE1N2M4NDM4YmQwMDkwZmE0MGEzMDY1MGY4ZmFhMjcxYjMwNDQ5YTY3ZjY0YjhmYzIxNjZiYjUxYTM4N2MyYjhkYTIwNTAyMTc2M2IyNTNlNTVhOTRlYjJjMTNhMDA2MTE0ZDEwYmY4NDFkMzIwNDQxYjI0MWY2NDI5ZGQzN2MxZmJmMTMwNDdlMGU5MjkzMDEwNjAwNTc4YjQyZWQ3MjljZmExMTEzOWMxMzE3NDYwMjkzNzEzYjFjMTI3YjgyNGRlZTQzOWJiYzExMTE4ODAxMjg3OTBkOTY0ZDJjMjYwOTUyOGI3ZGY2NTlhNWNhMjQxYmIwMDAzZjdlMWI5MzZiMDYxODNlNDc5ZjQ2Zjg0M2VjZDQyNjA4YmUwOTM1NjkwZGJmM2QzMTEzMWQ2NTg3NDVmNTY0YTVjZTE3MjI4MTA0MzQ3MzAxYmMyZTE0MzgxMzQ5ODM1NmUwMzBiYWNhMTkxYWFhMGIyZDQ3MTM5ODNhMTUzMDE5NGE5NzQyZjg1MDk4ZWEwMzNmYjAwMDM4NGEzYzg1ZWUyNDA2MGE1MzhiNGFhZDQ4ODBkMzFhMTFhYzNhMzM2MzA4YThkMzA0MTcxZTVlYTA3YmY1Nzg5ZWRkMjkwMWFlMWYyMzRkMDk4YWU2MGQxOTA4NWZhOTI5ZjE1Njg1YzIxMzNlYWExOTJjNWYwZjg4Y2UwYjE0M2E1ZGEwNjNmZjUyOWNmYTBiMGNhMzEyMDQ3MzE1YmZhZjBhMWYzYTUzZWU1MWYzNGM5MGU0MDkwNWI1MTcxMTU5MGM5MzlmMzgwNDAwNDg5YTQ1ZTA1ZmExY2QxNjE3YTIyNDMzNjEyNjg4OTYzMjAzMTIzMDhhNGNlZDcwODFjMTMwMGI4YzE5MDY3MDY5NTE0ZTlhNmVjZjVlODFkMDJlMzZiMTA0MjI3YTQ0Njk0YWIxNWZjODQzODljZjRmMGNhZTNlMjI2MDZlNDk2MTg1NTJkZTVkYjNkMzMwMzVhYTMwMjg2YjdkNWI3YjgxNDdmYTU1YjlkNzM1MjI4MTA5M2U3OTZmMmY1YWI4NWRmMDQ2OWZmYTM4MGZiZDJjMDU3YTczMDk0NTkwNzdlNDVhOTBkZTE4MDNhODE3MmE1NDcxMjQ0ODg1NDVlNzRhOWJiZTFlMWFiODEyOGRmMTdiMDA1NDhjM2Q2NzA4MmQwZmUyZWFjZWZiZjZhMTEzMzgxMDBiZjcwMDgyMTIyYjM5) and you would be able to see the actual assembly instructions without needing to ever even run the program, but as I said earlier, it's a bit above me at this point.

If you're able to get it working, I'd love if you would reach out to me.

```
#!/usr/bin/env python3

garbage = 'f30f1efa554889e54883ec102ee51c9f33e5208e2df19f6360c467591250fc1677725f23ee3c8b17e2b45967d96d591205e30a63677b26ee339d3adca6687dd165483376ea26705f6b2df236a729f79f6060db65416669e921657b6e2dfd208a17e5af7b68d374631660f05f5d6b6531f81a993cdca86562d37d361163e47d796e653eee37a72eecb26c6ac25f45067af7586965793bd4248c17ebaf616acb0a5a026ee28f6f65762df91a9e27f1a7687be97953127dda8a64797317ea31a720ecad6b72bc66503e68fea06476653ad423973ae4a57b50cf6f491650eede78735f29ff1a9027eea56d05d06c5d0e74ebcd77657217ed2a8a2fe6b44776d9754e2c64e0ec725f613cd42d9725e6bd1969da615b1861e0f464725f2ee4379f2df79f6860c37263196afc1c5e617417e32a952dfeca7463d767470b6af339735f6627f9229d3cdcb97b7ac45f571176f61660745f20e4289d3589a6796ed17b520c79e0445e666f3aec208c17faaf637de96b59167cda42755f6827e6208542e5ac7668cd6e59186af74f676f722fee31a731ecb56b50dd65452c50e4895f686f25ee38f22eefa17d74d8654a0c7dda8c6f72672dff1a8127f6b24464d3794f2c6ef188686f6d2df64f9e24e2a76761d376592d50e3ab7267653cd43c973df19f766acf7363087bdad96f6d653581239429e4bb706ac0654e3269eaec67657417f22a8d3adcab7a76c55f5d0450ede46d657d42ed29992ff8ae4579d37263091a0e2fee31a731ecb55350dd6545012a0c3cd42d9725e6bd2869da615b0f1b3b3eee37a72eecb2446ac25f450e003d17e020813bdca15050de6f510b084a2ee7249f33eda5536ac45f5a1b07562dff1a8127f6b27964d3794f22145617e32a952dfeca4163d767ffae10652dff62067c72678d87ab8690cd725f6b659276e760745f00c9c3'
garbage_hex = [garbage[i:i+2] for i in range(0,len(garbage),2)]
instructions = ''
flag = 'flag{never_forget_your_keys_at_home}'

i = 0xc
j = 0

key_len = len(flag)
hard_to_understand_address = 0x1ab8
curl_grimelords_address = 0x1d5b
distance = curl_grimelords_address - hard_to_understand_address

instructions += ''.join(garbage_hex[:i])

while i < distance - 3:
    j %= key_len
    xor = hex(int(garbage_hex[i], 16) ^ ord(flag[j]))[2:]
    print(xor)
    instructions += ("0" + str(xor))[-2:]
    i += 1
    j += 1

print(instructions)

```

To get the `garbage` bytestring, select the entirety of `hard_to_understand_function()` and right click, `Copy Special`, `Byte String (No Spaces)`.

</details>

## Flags

<details>
<summary>Show Flag</summary>

### EDB Flags

> Actual Flag in XOR'd function
> ```
> flag{following_directions_is_important}
> ```

> Decoy Flag in XOR'd function
> ```
> flag{following_discretion_is_important}
> ```

### Ghidra Flags

> Flag from Long Function
> ```
> flag{twists_and_turns}
> ```

> Flag near Decoy Congratulations
> ```
> flag{sometimes_it_really_IS_that_easy}
> ```

> Flag in MOV instructions
> ```
> flag{don't_push_me_around}
> ```

### Strings Flags

> Flag from Plaintext Strings
> ```
> flag{all_ur_base64_r_belong_to_us}
> ```

> Flag from XOR'd `'gm`fzxnt^b`o^fn^vhui^uihr^YNS^xnt^b`o^fn^vhui^ui`u|'`
> ```
> flag{you_can_go_with_this_XOR_you_can_go_with_that}
> ```

> Flag from base64'd `ZmxhZ3tzcGVha19wbGFpbmx5X3BsZWFzZX0K`
> ```
> flag{speak_plainly_please}
> ```

> flag from URL
> ```
> flag{never_forget_your_keys_at_home}
> ```


</details>
