# A Tale of Two Spies (100 Points)

## Description

You suspect one of your employees (Alice) is exfiltrating data from your organization’s San Antonio branch. After investigating her work mailbox, you discover she has been communicating with an unknown person (Bob). The contents of Bob’s latest email to Alice makes it sound like they will meet soon.

In this email, Bob attached 2 documents (1 .docx file and 1 .wav file). Although both files appear legitimate, there is something suspicious about this latest communication. Investigate the 2 files to see if there is any additional information that could be useful to stopping this activity.

First question: What is the name of the hidden image in the .docx file?

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* unzip

### Walkthrough

`.docx` files are actually just `.zip` files that Microsoft Word knows how to deal with. If you make a copy of the `.docx` with the `.zip` extension, then unzip it and go to `Email form Bob/word/media/`, you will see two images. One is the one you'll recognize from the document, the other is shown below:

![](../../images/a_tale_of_two_spies_1-001.png)

This image is named `image1.png`

</details>

## Flags

<details>
<summary>Show Flag</summary>

> Name of the hidden image
> ```
> image1.png
> ```

</details>
