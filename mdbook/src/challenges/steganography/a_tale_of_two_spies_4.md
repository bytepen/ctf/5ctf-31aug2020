# A Tale of Two Spies 4 (50 Points)

## Description

What tool will desteg the .wav file?

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* Google

### Walkthrough

After examining `image1.png` below, I didn't recognize the program. A quick Google Image Reverse Search revealed that this was `OpenPuff`, or `openpuff` in the styling of the flag.

![](../../images/a_tale_of_two_spies_1-001.png)

</details>

## Flags

<details>
<summary>Show Flag</summary>

> What tool will desteg the .wav file?
> ```
> openpuff
> ```

</details>
