# A Tale of Two Spies 5 (100 Points)

## Description

What was the password used to desteg the hidden file?

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* OpenPuff

### Walkthrough

Looking at the document, we see a reall weird address:

> It’ll probably be that Starbucks on 78209199 Walzem Road, but we might switch to somewhere else.

What street address has 8 numbers? Also, if we look at the image below, we will notice there are 8 characters in the password:

![](../../images/a_tale_of_two_spies_1-001.png)

This makes it pretty obvious that the password is `78209199`.

</details>

## Flags

<details>
<summary>Show Flag</summary>

> What is the password?
> ```
> 78209199
> ```

</details>
