# A Tale of Two Spies 6 (80 Points)

## Description

What is the spoken address in the .mp3 file?

## Solution

<details>
<summary>Show Solution</summary>

### Tools

* Audacity

### Walkthrough

After extracting the `Noname.mp3` with OpenPuff in the previous challenge, we open it up in `Audacity` and it sounds backwards. Additionally, the `.docx` has the hint "let me know if it sounds backwards or anything.". We highlight all the audio and select `Effects` => `Reverse`. Playing it reveals `Jump Street.... 37 Jump Street`.

</details>

## Flags

<details>
<summary>Show Flag</summary>

> What's the address?
> ```
> 37 Jump Street
> ```

</details>
