# Steganography

This category only had 6 challenges, but there's more steganography in other categories, such as forensics.
These challenges were worth a total of 430 Points.

I thought these challenges were pretty neat.
I'd always known that `.docx` files were ZIPs, but I never REALLY looked inside of them.
I also got to play around with a handful of new tools since I'd never done steganography with audio files before.
