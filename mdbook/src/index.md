# 5CTF

## About this Document

This document was created by myself while/after going through the 5CTF.
I have completed many walkthroughs, but I may or may not get around to finishing all of them.
If the page does not have a description, there is no walkthrough, but if you see a description, you can consider that walkthrough final.

I went through and put an asterisk (`*`) next to each challenge in the table of contents that does not have a complete write-up so you can easily tell.
If there are any you'd like to see completed that I have not done, please feel free to submit an issue on the GitLab page linked in the top right.

Most challenge files can be found [here](https://gitlab.com/bytepen/ctf/5ctf-31aug2020/-/tree/master/challenge-files).
The names of the files SHOULD correspond to the challenge.

### Showing Solutions

I prefer to keep the solutions hidden unless you purposfully want to view them.
You can view them by clicking on the "Show Solution" dropdown on each page.
Alternatively, clicking the button below will toggle whether they are open/closed by default.

<button id='toggle-solutions-button' onclick='toggleSolutions()'></button>

## About the CTF

This page covers the 5CTF held from 31AUG2020 to 11SEP2020.
This CTF was split into two realms; Solo and Team.

There were 155 total participants, 177 challenges, and 14 organizations that participated in this CTF.

## Scores

### Solo
| Place | Player   | Points |
| ----- | ------   | ------ |
| 1     | bamhm182 | 8800   |
| 2     | [hidden] | 7558   |
| 3     | [hidden] | 6450   |
| 4     | [hidden] | 5890   |
| 5     | [hidden] | 5110   |
| ...   | ...      | ...    |


### Team

| Place | Team     | Points |
| ----- | -------- | ------ |
| 1     | [hidden] | 9340   |
| 2     | [hidden] | 9254   |
| 3     | [hidden] | 9200   |
| 4     | [hidden] | 8500   |
| 5     | [hidden] | 6008   |
| ...   | ...      | ...    |
