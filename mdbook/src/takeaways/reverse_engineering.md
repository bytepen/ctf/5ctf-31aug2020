# Reverse Engineering

This was my first time REALLY digging into Reverse Engineering, I learned a TON from the CTF and hope to impart some of that knowledge upon whoever reads this.

I was originally planning on trying to walk through examples of each of the items on this page, but I think instead I will use this page to give you a brief overview of each item to give you an idea, and then fully walk through the tools and my thought process in each Reverse Engineering challenge.
I recommend you read this page first, then start at the top of the Reverse Engineering category and work your way down.
I'm hoping this will allow the thing I talk about on this page to make more sense and help solidify them through actual examples.

That said, I do want to provide SOMETHING to look at here, so I will be referencing the hardest RE challenge in this CTF; `TrollyMcTrollFace`.
This challenge required me to use all of the tools I really go into depth about here.

## Assembly

One of the most important skills that I was able to take away from this CTF is the ability to not be 100% intimidated by assembly.
I'm not going to even pretend like I understand all the different registers and memory locations and how they all work together, but as long as you acknowledge that it's okay to not have a real clue what's going on and just start trying to make sense of patterns and actions that happen when certain assembly is run, your brain will start to pick up on those patterns and it will slowly become easier to get somewhat of a clue as to what is going on.

It's a very strong belief of mine that if you believe you are unable to learn something, you will subconciously block yourself from learning it and your belief will become reality.
Therefore, with just accepting the fact that you have absolutely no idea what's going on and acknowleding that it is fine, your brain will do a lot of heavy lifting in the background to pull it all together.

## Ghidra

Ghidra was one of the two tools that I used extensively during RE challenges for this CTF. The other was EDB (discussed below). I plan on getting really into Ghidra from a complete n00b's perspective, so buckle up, buckeroo!

First off, Ghidra is an open source tool that was released by the NSA a while back that offers a TON of functionality when it comes to attempting to decompile and reverse engineer programs.
Some of the core functionalities that I took advantage of within the context of the CTF were the ability to load in a compiled program and view its best attempt at recreating the original source code.
In many circumstances, it is very hard to read the decompiled code it gives you, or the code isn't available at all.
In those circumstances, it really pays off to be able to understand assembly.
I currently really suck at this, but I learned a lot from this CTF and hope to try and demystify it a little here.

One other functionality that proved very useful is the ability to `patch` programs.
Essentially, this means that if some action is taken within a program, you can sometimes change the action and make it do something you want instead.
As an example, if you're expected to enter a password and there is some code like `if (my_guess[i] == real_pass[i]) {...}`, you can change the assembly so it evaluates as `if (my_guess[i] != real_pass[i]) {...}` and then all you have to do is get the password 100% wrong instead of 100% right.
One very important thing to note here is that there are two ways to import a Linux `ELF` program; as `RAW` and as `ELF`.
I was unable to patch programs imported as `ELF` and needed to import the program as `RAW` to patch it.
I'll get into that in a bit.

To continue on with this idea of importing as `ELF` vs `RAW`, I found that it is very helpful to actually import both as `ELF` and `RAW`.
The reason for this is that I found some things decompiled better in `ELF` and some things decompiled better in `RAW`.
Additionally, for some reason, even Ghidra's ability to understand what the code was doing and represent it as assembly seemed to vary sometimes between `ELF` and `RAW`.
With it open in both forms and each of them providing different pieces to the puzzle, they can both be used as reference while walking through the program either statically with Ghidra or dynamically with the help of `edb`, `gdb`, or `strace`.

Finally, when you start digging into some really complex programs (especially when they don't contain their debugging symbols), ghidra doesn't have much to go on as far as function and variable names.
Clicking on something like `FUN_0001abc` and clicking the `l` key will enable you to rename this into something more understandable.
If you're looking around at a program in ghidra for more than just a few minutes, it's highly beneficial to label everything you understand as soon as you understand it.

## EDB (Evan's Debugger)

edb is a Linux tool that is similar to OllyDB (Windows equivalent not covered here because all RE challenges that actually executed were Linux).
This tool lets you load in a program and step through it one instruction at a time.
I found it enormously helpful and it ended up being the key to figuring out what exactly was going on in `TrollyMcTrollFace`.

The "hard part" about EDB is that there is no attempt at decompilation of the assembly into code.
That means you really need to be able to have a basic understanding of what is going on in the assembly.
If you do not, you can attempt to match up the code that is executing with the assembly in Ghidra and hope that the code is somewhat understandable there.

## Strace

`strace` is definitely a great tool to add to your RE toolbelt.
With strace, you can specify a program you wish to inspect, and it will dump VERY verbose information to the screen that basically tells you anything it's doing, any system calls its makeing, and any file it is trying to access.

When working on `TrollyMcTrollFace`, I really started to gain some traction when I started running `strace ./TrollyMcTrollFace` as I was able to easily see behind the scenes to determine what it's doing when I perform certain actions.
What's better is that with something like `strace -p $(ps -ef | grep 'TrollyMcTrollFace' | cut -d" " -f2)` while running it in `edb` and see what it's doing one step at a time.

If you look at the example below, you can see that the flag starting with `flag{never_forget_your_keys_at_h` was written to a file that was then closed.
After that, a file `/tmp/flag.txt` was opened and that flag was read.
If you didn't know that flag was there, that would be a very easy way to spot it.

![](../../images/takeaways_re_strace.png)

## GDB (The GNU Project Debugger)

I thought I would mentioned GDB here because if you know how to use it, it should be incredibly helpful and *may* be more helpful/powerful than other solutions.
That said, it had a high enough learning curve and the challenge I was trying to use it on was complex enough (TrollyMcTrollFace) that I had zero luck with it.
If you're reading this in hopes to better understand Reverse Engineering Linux application, just note that it is probably worth your time to sit down and figure out how GDB works.
