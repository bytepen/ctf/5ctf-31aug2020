# RSA

There were a lot of RSA challenges in this CTF. I'm going to go into depth on RSA and how its values work together here for reference during challenges. Buckle up, because if you're not good at math, this may make your brain hurt a bit. That said, I'm trying to keep this as understandable as possible (unlike most of the online references I found while trying to understand this).

## What is RSA?

RSA is a asymetric Public Key Infrastructure (PKI) algorithm, meaning that the public key is different from the private key. If you've set up SSH securely, you've almost certainly used the command `ssh-keygen` at some point. This command (by default) generates an RSA keypair for you that may be recognizable as something like the following:

> Private Key
> ```
> -----BEGIN OPENSSH PRIVATE KEY-----
> b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAAAlwAAAAdzc2gtcn
> NhAAAAAwEAAQAAAIEA1qqCdv0US2o+KUVbHBUAf0sLwyDIWDTCxgkN/K2W6DrFOnswSIl/
> UAjMXAlk9Rjy9YOaj0f15Ix4QORVI3NjCjT+9JRELvtEBG4nkpWJbAzLnMpirElhzGR+bk
> IKPRa/tcdyfWje+wkofo7rQeoYhBVA7g8IkZNuWV+UpK3MiCkAAAIIJme8NCZnvDQAAAAH
> c3NoLXJzYQAAAIEA1qqCdv0US2o+KUVbHBUAf0sLwyDIWDTCxgkN/K2W6DrFOnswSIl/UA
> jMXAlk9Rjy9YOaj0f15Ix4QORVI3NjCjT+9JRELvtEBG4nkpWJbAzLnMpirElhzGR+bkIK
> PRa/tcdyfWje+wkofo7rQeoYhBVA7g8IkZNuWV+UpK3MiCkAAAADAQABAAAAgF0gJW8XRY
> md8o+W7fqVxdxzIwwomaFiaijdXrsCV5gxfsRrErw1Ig0HdBhtXuXRR/1odVG5Rxw6MCT+
> qUyyzwCfuEI/IUFvERHPzIufTcwV5YokpKnFAtj9I0S/XqsuCQGD6VtGe+WCOxSxe4C4hw
> x0fmO/yrwRZ49ZDNK0e3U9AAAAQDxxb/b77VrXqPUsf8Ndba8U6OH9qcd9O0Oe/0nXJJrK
> 3T8UvLiXKT301nYMN1vnoPGgOE4kAQZ0hSqo+hlERCMAAABBAPqwrX7tbt+ZrzfW6Ck5mx
> k0fvBFzC2H8kPLKmlURAFNNhTqQMjh25U4Ns/rr1+2UZBIANFhziKHrmsoGJYkzcsAAABB
> ANs2f2tjVVLdrRzm4UKngVIVlwwyOtRg7xStYJUVypRXsnttYmrRLFPsU/yfJyxtHqIVo5
> yEEkEHXU47HbcqA1sAAAAOYmFtaG0xODJAUm90b20BAgMEBQ==
> -----END OPENSSH PRIVATE KEY-----
> ```

> Public Key
> ```
> ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAAgQDWqoJ2/RRLaj4pRVscFQB/SwvDIMhYNMLGCQ38rZboOsU6ezBIiX9QCMxcCWT1GPL1g5qPR/XkjHhA5FUjc2MKNP70lEQu+0QEbieSlYlsDMucymKsSWHMZH5uQgo9Fr+1x3J9aN77CSh+jutB6hiEFUDuDwiRk25ZX5SkrcyIKQ== user@host
> ```

## RSA Components

What you may be unaware of is that these keys are actually just easy (for humans) ways to convey a set of mathmatically related numbers. The primary numbers that are dealt with in RSA are as follows:

* n (modulus): This is p*q
* e (publicExponent): This is the number used to encrypt messages. This is most commonly `3` or `65537`
* d (privateExponent): This is the number used to decrypt messages
* p (prime1): This is a secret prime number generated in the creation of the key
* q (prime2): This is also a secret prime number generated in the creation of the key

There's also the following that can be derived from the above:

* r (phi): This is (p - 1) * (q - 1) and I don't know exactly what it is if I'm being honest.

The `private` key contains all the above numbers, while the `public` key only contains `n` and `e`.

## RSA Component Equations

Finally, there are certain equations that can help you derive the other components if a few of them are known. A few of them are shown below:

```
n = p*q
r = (p - 1) * (q - 1)
e = d - 1 mod phi
d = e - 1 mod phi
p = n // q
q = n // p
```

Note that in `python3`, you will get `TypeError: 'float' object cannot be interpreted as an integer` if you do `hex(n / q)`. `hex(n / q)` works in `python2`, but you need `hex(n // q)` in `python3`.

The hardest equation for me to understand was how to get to `e` and `d`. This isn't `e = d - (1 % phi)` as I was originally trying to understand it, but instead it is what's known as a `modular inverse`. I found the following python code a lot online that gives you the `modular inverse` of two numbers:


```
def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    else:
        g, y, x = egcd(b % a, a)
    return (g, x - (b // a) * y, y)

def modinv(a, m):
    g, x, y = egcd(a, m)
    if g != 1:
        raise Exception('modular inverse does not exist')
    else:
        return x % m

```

This makes the equations `e = modinv(d, r)` and `e = modinv(e, r)`. I still don't fully understand what it is, but this works, so I'm cool with it.

## View the RSA Components

To help understand these, I recommend that you run the following commands to get an RSA keypair to inspect for yourself.

```
openssl genrsa -out key.pem 1024
openssl rsa -in key.pem -pubout -out pub.pem
```

This gave me the following:

> Private Key
>
> ```
> -----BEGIN RSA PRIVATE KEY-----
> MIICWwIBAAKBgQCr7t5/sxv1Ly4saGs6IuIG/DgoDGNCjn7lr+XUeNkazDMPzmHe
> BOvmXnz3n3kxpElP25oj9DKgMZuZ1JMtRFw7jVObyEOdrYb2LgKHpxCEigDSY/c+
> 1Uq7Q06bPRNMJyYYGmDBhDYijIIf4D578yRQwBePG73YJ6+kAuz74hplPwIDAQAB
> AoGAGHi3UpW7tPoyKD/0gyHT6gqcM5cPyblednLC06sfu6C049sMMv2gVfm878HT
> Di3EAYnQNq3bRtHIw+xtxlOkjNmnkEgyvzQEZZvIiHM4Pl+2wdD1p9tgrlN2t1av
> AHKxydlsAUVswiZKGm6dyHJBp0GWz98UCCn9pux1+vC1gYECQQDS4SL5gGygmwoG
> 4FkcV483GUWFcp1iAqv/RwQwMpJ0WjXhmOVWxeSbMRYNvjirLez7oxCufxk4XoyN
> 3YuxrYqVAkEA0Lh03LsVo+A90m/JqeFQADYbzoFQoEZ52vHG/ASJWZiaVusiZUJd
> 6fRd5EdsjmG1yqfUVIxE6sQc41yEpMbPgwJAUbk6SqMAdgVfj2amYht0vw+mJ59R
> ashKS7YD44HVzA2hz/nm9wfeHl6Zg13yrDQyIRsCvf03kubhZaE0H/l3OQJAEwxS
> dfaI8gy796GGGP/cCjjl7sG0dvwXkNix3TwYwNmBdF8/I1icM4a+rt+p05CtQ5Jv
> eUvX2kkNjd5aqKMQOwJAO8MpFsWZ5Yo3c3jWH5412oROllADTw+NJAkQV4xeesUt
> H15D3ZPe+wzrl3VncTa4TRQlIBLPx5CVdP/Mp6YAiQ==
> -----END RSA PRIVATE KEY-----
> ```
>
> Public Key
>
> ```
> -----BEGIN PUBLIC KEY-----
> MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCr7t5/sxv1Ly4saGs6IuIG/Dgo
> DGNCjn7lr+XUeNkazDMPzmHeBOvmXnz3n3kxpElP25oj9DKgMZuZ1JMtRFw7jVOb
> yEOdrYb2LgKHpxCEigDSY/c+1Uq7Q06bPRNMJyYYGmDBhDYijIIf4D578yRQwBeP
> G73YJ6+kAuz74hplPwIDAQAB
> -----END PUBLIC KEY-----
> ```

You can see the values within these keys by running the following commands:

```
openssl rsa -in key.pem -text -noout
openssl rsa -in pub.pem -pubin -text -noout
```

> Private Key
> ```
> RSA Private-Key: (1024 bit, 2 primes)
> modulus:
>     00:ab:ee:de:7f:b3:1b:f5:2f:2e:2c:68:6b:3a:22:
>     e2:06:fc:38:28:0c:63:42:8e:7e:e5:af:e5:d4:78:
>     d9:1a:cc:33:0f:ce:61:de:04:eb:e6:5e:7c:f7:9f:
>     79:31:a4:49:4f:db:9a:23:f4:32:a0:31:9b:99:d4:
>     93:2d:44:5c:3b:8d:53:9b:c8:43:9d:ad:86:f6:2e:
>     02:87:a7:10:84:8a:00:d2:63:f7:3e:d5:4a:bb:43:
>     4e:9b:3d:13:4c:27:26:18:1a:60:c1:84:36:22:8c:
>     82:1f:e0:3e:7b:f3:24:50:c0:17:8f:1b:bd:d8:27:
>     af:a4:02:ec:fb:e2:1a:65:3f
> publicExponent: 65537 (0x10001)
> privateExponent:
>     18:78:b7:52:95:bb:b4:fa:32:28:3f:f4:83:21:d3:
>     ea:0a:9c:33:97:0f:c9:b9:5e:76:72:c2:d3:ab:1f:
>     bb:a0:b4:e3:db:0c:32:fd:a0:55:f9:bc:ef:c1:d3:
>     0e:2d:c4:01:89:d0:36:ad:db:46:d1:c8:c3:ec:6d:
>     c6:53:a4:8c:d9:a7:90:48:32:bf:34:04:65:9b:c8:
>     88:73:38:3e:5f:b6:c1:d0:f5:a7:db:60:ae:53:76:
>     b7:56:af:00:72:b1:c9:d9:6c:01:45:6c:c2:26:4a:
>     1a:6e:9d:c8:72:41:a7:41:96:cf:df:14:08:29:fd:
>     a6:ec:75:fa:f0:b5:81:81
> prime1:
>     00:d2:e1:22:f9:80:6c:a0:9b:0a:06:e0:59:1c:57:
>     8f:37:19:45:85:72:9d:62:02:ab:ff:47:04:30:32:
>     92:74:5a:35:e1:98:e5:56:c5:e4:9b:31:16:0d:be:
>     38:ab:2d:ec:fb:a3:10:ae:7f:19:38:5e:8c:8d:dd:
>     8b:b1:ad:8a:95
> prime2:
>     00:d0:b8:74:dc:bb:15:a3:e0:3d:d2:6f:c9:a9:e1:
>     50:00:36:1b:ce:81:50:a0:46:79:da:f1:c6:fc:04:
>     89:59:98:9a:56:eb:22:65:42:5d:e9:f4:5d:e4:47:
>     6c:8e:61:b5:ca:a7:d4:54:8c:44:ea:c4:1c:e3:5c:
>     84:a4:c6:cf:83
> exponent1:
>     51:b9:3a:4a:a3:00:76:05:5f:8f:66:a6:62:1b:74:
>     bf:0f:a6:27:9f:51:6a:c8:4a:4b:b6:03:e3:81:d5:
>     cc:0d:a1:cf:f9:e6:f7:07:de:1e:5e:99:83:5d:f2:
>     ac:34:32:21:1b:02:bd:fd:37:92:e6:e1:65:a1:34:
>     1f:f9:77:39
> exponent2:
>     13:0c:52:75:f6:88:f2:0c:bb:f7:a1:86:18:ff:dc:
>     0a:38:e5:ee:c1:b4:76:fc:17:90:d8:b1:dd:3c:18:
>     c0:d9:81:74:5f:3f:23:58:9c:33:86:be:ae:df:a9:
>     d3:90:ad:43:92:6f:79:4b:d7:da:49:0d:8d:de:5a:
>     a8:a3:10:3b
> coefficient:
>     3b:c3:29:16:c5:99:e5:8a:37:73:78:d6:1f:9e:35:
>     da:84:4e:96:50:03:4f:0f:8d:24:09:10:57:8c:5e:
>     7a:c5:2d:1f:5e:43:dd:93:de:fb:0c:eb:97:75:67:
>     71:36:b8:4d:14:25:20:12:cf:c7:90:95:74:ff:cc:
>     a7:a6:00:89
> ```
>
> Public Key
>
> ```
> RSA Public-Key: (1024 bit)
> Modulus:
>     00:ab:ee:de:7f:b3:1b:f5:2f:2e:2c:68:6b:3a:22:
>     e2:06:fc:38:28:0c:63:42:8e:7e:e5:af:e5:d4:78:
>     d9:1a:cc:33:0f:ce:61:de:04:eb:e6:5e:7c:f7:9f:
>     79:31:a4:49:4f:db:9a:23:f4:32:a0:31:9b:99:d4:
>     93:2d:44:5c:3b:8d:53:9b:c8:43:9d:ad:86:f6:2e:
>     02:87:a7:10:84:8a:00:d2:63:f7:3e:d5:4a:bb:43:
>     4e:9b:3d:13:4c:27:26:18:1a:60:c1:84:36:22:8c:
>     82:1f:e0:3e:7b:f3:24:50:c0:17:8f:1b:bd:d8:27:
>     af:a4:02:ec:fb:e2:1a:65:3f
> Exponent: 65537 (0x10001)
> ```

I found that the easiest way to mess with these is with python. You will need `pycryptodome`, so make sure you `pip3 install pycryptodome` if you don't already have it. After that, import them with the following commands:

```
from Crypto.PublicKey import RSA
pri = RSA.importKey(open('key.pem').read())
pub = RSA.importKey(open('pub.pem').read())
```

Now you can inspect the various numbers by executing commands such as `pri.q`.

Furthermore, if you have the keys in python variables as is shown below, you can create the private and public keys within python with the `RSA.construct()` function.

```
from Crypto.PublicKey import RSA

n = 0xabeede7fb31bf52f2e2c686b3a22e206fc38280c63428e7ee5afe5d478d91acc330fce61de04ebe65e7cf79f7931a4494fdb9a23f432a0319b99d4932d445c3b8d539bc8439dad86f62e0287a710848a00d263f73ed54abb434e9b3d134c2726181a60c18436228c821fe03e7bf32450c0178f1bbdd827afa402ecfbe21a653f
e = 0x10001
d = 0x1878b75295bbb4fa32283ff48321d3ea0a9c33970fc9b95e7672c2d3ab1fbba0b4e3db0c32fda055f9bcefc1d30e2dc40189d036addb46d1c8c3ec6dc653a48cd9a7904832bf3404659bc88873383e5fb6c1d0f5a7db60ae5376b756af0072b1c9d96c01456cc2264a1a6e9dc87241a74196cfdf140829fda6ec75faf0b58181
p = 0xd2e122f9806ca09b0a06e0591c578f37194585729d6202abff4704303292745a35e198e556c5e49b31160dbe38ab2decfba310ae7f19385e8c8ddd8bb1ad8a95
q = 0xd0b874dcbb15a3e03dd26fc9a9e15000361bce8150a04679daf1c6fc048959989a56eb2265425de9f45de4476c8e61b5caa7d4548c44eac41ce35c84a4c6cf83

pri = RSA.construct((n,e,d,p,q))
pub = RSA.construct((n,e))
```

## Encryption & Decryption

You can also encrypt and decrypt messages with this as follows:

```
ciphertext = pub.encrypt(b'hello, world!', 0)
plaintext = pri.decrypt(ciphertext)
```

Note that these deal with bytestrings which can be hard to transmit, so the challenges here work with hex. These bytestrings can use hex via the following commands instead.

```
ciphertext = pub.encrypt(b'hello, world!', 0)[0].hex()
plaintext = bytes.fromhex(hex(pri.decrypt(ciphertext))[2:])
```

Additionally, so long as you have the required RSA components added as python variables as shown above, you can do this with simple math.

```
plaintext = int(b'this_is_a_test'.hex(), 16)
ciphertext = pow(plaintext, e, n)
print(hex(ciphertext))
decrypted = pow(ciphertext, d, n)
print(bytes.fromhex(hex(decrypted)[2:]))
```

Note that the `pow(a,b,c)` function is the same as `(a ** b) % n` (`(a ^ b) % n`), but it is often MUCH quicker.

## RsaCtfTool

While I was digging around for information on this subject, I found the [RsaCtfTool](https://github.com/Ganapati/RsaCtfTool), which came in very handy in some of the challenges. This tool will let you provide public keys and attempt to attack for private key, dump public and private keys, and all sorts of other super neat things. There are a ton of attacks you ca n do to try and get a private key from a public key. I will say, though, that sometimes it does provide false positive, so be sure to check the answers that it gives you before submitting them.

Hopefully this information helps you make sense of RSA and you're able to use it to solve some of the challenges in this CTF!
